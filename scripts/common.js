﻿function showPreloader(){
    $('#preloader').css('display', 'block');
}
function hidePreloader() {
    $('#preloader').css('display', 'none');
}
function addRestaurant() {
    showPreloader();
    var restaurantName = $("#restaurantName").val();
    var ownerName = $("#ownerName").val();
    var establishYear = $("#establishYear").val();
    var cuisineService = $("#cuisineService").val();
    var email = $("#email").val();
    var contactNumber = $("#contactNumber").val();
    var restaurantAddress = $("#restaurantAddress").val();
    if (restaurantName != ""){
        if (ownerName != ""){
            if (establishYear != ""){
                if (cuisineService != ""){
                    if (email != ""){
                        if (contactNumber != ""){
                            if (restaurantAddress != ""){
                                var quotations = {
                                    restaurantName: restaurantName,
                                    ownerName: ownerName,
                                    establishYear: establishYear,
                                    cuisineService: cuisineService,
                                    contactNumber: contactNumber,
                                    email: email,
                                    restaurantAddress: restaurantAddress,
                                    id: 0
                                };
                                $.ajax({
                                    type: 'POST',
                                    url: tempBaseURL + 'restaurantreport/sendrestaurantrequest',
                                    beforeSend: function (xhdr) {
                                        xhdr.overrideMimeType("application/j-son;charset=UTF-8")
                                        xhdr.setRequestHeader("Authorization", 'Bearer ' + localStorage.getItem('accessToken'));
                                    },
                                    data: JSON.stringify(quotations),
                                    dataType: 'text',
                                    contentType: 'application/json; charset=utf-8',
                                    success: function (data) {
                                        hidePreloader();
                                        location.reload();
                                    },
                                    error: function (data) {
                                        hidePreloader();
                                        alert("error " + data.status);
                                        return false;
                                    }
                                });
                            }
                            else {
                                if (bool == true) {
                                    showmsg('danger', 'glyphicon glyphicon-warning-sign', '  ', 'حقل عنوان المطعم مطلوب');
                                } else {
                                    showmsg('danger', 'glyphicon glyphicon-warning-sign', '  ', 'The restaurant address field is required');
                                }
                                hidePreloader();
                            }
                        }
                        else {
                            if (bool == true) {
                                showmsg('danger', 'glyphicon glyphicon-warning-sign', '  ', 'حقل رقم جهة الاتصال مطلوب');
                            } else {
                                showmsg('danger', 'glyphicon glyphicon-warning-sign', '  ', 'The contact number field is required');
                            }
                            hidePreloader();
                        }
                    }
                    else {
                        if (bool == true) {
                            showmsg('danger', 'glyphicon glyphicon-warning-sign', '  ', 'حقل معرف البريد الإلكتروني مطلوب');
                        } else {
                            showmsg('danger', 'glyphicon glyphicon-warning-sign', '  ', 'The email id field is required');
                        }
                        hidePreloader();
                    }
                }
                else {
                    if (bool == true) {
                        showmsg('danger', 'glyphicon glyphicon-warning-sign', '  ', 'مطلوب مجال خدمة المأكولات');
                    } else {
                        showmsg('danger', 'glyphicon glyphicon-warning-sign', '  ', 'The cuisine service field is required');
                    }
                    hidePreloader();
                }
            }
            else {
                if (bool == true) {
                    showmsg('danger', 'glyphicon glyphicon-warning-sign', '  ', 'حقل السنة المحددة مطلوب');
                } else {
                    showmsg('danger', 'glyphicon glyphicon-warning-sign', '  ', 'The established year field is required');
                }
                hidePreloader();
            }
        }
        else {
            if (bool == true) {
                showmsg('danger', 'glyphicon glyphicon-warning-sign', '  ', 'حقل اسم المالك مطلوب');
            } else {
                showmsg('danger', 'glyphicon glyphicon-warning-sign', '  ', 'The owner name field is required');
            }
            hidePreloader();
        }
    }
    else {
        if (bool == true) {
            showmsg('danger', 'glyphicon glyphicon-warning-sign', '  ', 'حقل اسم المطعم مطلوب');
        } else {
            showmsg('danger', 'glyphicon glyphicon-warning-sign', '  ', 'The restaurant name field is required');
        }
        hidePreloader();
    }
}

function resetAddRestaurant() {
    $('#addRestaurant').modal('toggle');
}

function contactUs() {
    showPreloader();
    var firstName = $("#firstName").val();
    var lastName = $("#lastName").val();
    var email = $("#conEmail").val();
    var phoneNo = $("#conPhoneNo").val();
    var address = $("#conAddress").val();
    if (firstName != "") {
        if (lastName != "") {
            if (email != "") {
                if (phoneNo != "") {
                    if (address != "") {
                        var cont = {
                            firstName: firstName,
                            lastName: lastName,
                            email: email,
                            phoneNo: phoneNo,
                            address: address,
                            id: 0
                        };
                        $.ajax({
                            type: 'POST',
                            url: tempBaseURL + 'restaurantreport/contactus',
                            beforeSend: function (xhdr) {
                                xhdr.overrideMimeType("application/j-son;charset=UTF-8")
                                xhdr.setRequestHeader("Authorization", 'Bearer ' + localStorage.getItem('accessToken'));
                            },
                            data: JSON.stringify(cont),
                            dataType: 'json',
                            contentType: 'application/json; charset=utf-8',
                            success: function (data) {
                                hidePreloader();
                                location.reload();
                            },
                            error: function (data) {
                                hidePreloader();
                                alert("error " + data.status);
                                return false;
                            }
                        });
                    }
                    else {
                        if (bool == true) {
                            showmsg('danger', 'glyphicon glyphicon-warning-sign', '  ', 'حقل العنوان مطلوب');
                        } else {
                            showmsg('danger', 'glyphicon glyphicon-warning-sign', '  ', 'The address field is required');
                        }
                        hidePreloader();
                    }
                }
                else {
                         if (bool == true) {
                             showmsg('danger', 'glyphicon glyphicon-warning-sign', '  ', 'حقل رقم الهاتف مطلوب');
                        } else {
                             showmsg('danger', 'glyphicon glyphicon-warning-sign', '  ', 'The phone number field is required');
                        }
                    
                    hidePreloader();
                }
            }
            else {
                if (bool == true) {
                    showmsg('danger', 'glyphicon glyphicon-warning-sign', '  ', 'حقل معرف البريد الإلكتروني مطلوب');
                } else {
                    showmsg('danger', 'glyphicon glyphicon-warning-sign', '  ', 'The email id field is required');
                }
                hidePreloader();
            }
        }
        else {
            if (bool == true) {
                showmsg('danger', 'glyphicon glyphicon-warning-sign', '  ', 'حقل اسم العائلة مطلوب');
            } else {
                showmsg('danger', 'glyphicon glyphicon-warning-sign', '  ', 'The last name field is required');
            }
            hidePreloader();
        }
    }
    else {
        if (bool == true) {
            showmsg('danger', 'glyphicon glyphicon-warning-sign', '  ', 'حقل الاسم الأول مطلوب');
        } else {
            showmsg('danger', 'glyphicon glyphicon-warning-sign', '  ', 'The first name field is required');
        } 
        hidePreloader();
    }
    
}

function resetContactUs(type) {
    if (type != "close") {
        if (loggedInUserName == "") {
            $('#contactUs').modal('toggle');
        }
        else {
            showPreloader();
            var userMailIdss = loggedInUserName.slice(0, -1);
            $.ajax({
                url: tempBaseURL + 'user/byemail/' + userMailIdss,
                beforeSend: function (xhdr) {
                    xhdr.overrideMimeType("application/j-son;charset=UTF-8")
                    xhdr.setRequestHeader("Authorization", 'Bearer ' + localStorage.getItem('accessToken'));
                },
                type: 'GET',
                data: {},
                dataType: 'json',
                contentType: 'application/json',
                success: function (data) {
                    var name = data.name;
                    var xx = name.indexOf(' ');
                    if (xx == -1) {
                        $("#firstName").val(name);
                    }
                    else {
                        var FnameLname = name.split(' ');
                        $("#firstName").val(FnameLname[0]);
                        $("#lastName").val(FnameLname[1]);
                    }
                    $("#conEmail").val(data.email);
                    $("#conPhoneNo").val(data.phoneNumber);

                    hidePreloader();
                    $('#contactUs').modal('toggle');
                },
                error: function (d) {
                }
            });
        }
    }
    else {
        $('#contactUs').modal('toggle');
    }
}

function changeLanguage(lan, LanChangeController) {
    showPreloader();
	var url = window.location.pathname.split("/");
	var urlToLoad="";
	var questions=LanChangeController;
	var bool=questions.endsWith("Ar");
	var ctrlName=questions;
    $('#langDd').html('');
    if (lan == 'English') {
        $('#langDd').html('English <span class="caret"  style="border-top-color: #fbf8f8;"></span>');
    }
    else if(lan == 'Arabic') {
        $('#langDd').html('عربي <span class="caret"  style="border-top-color: #fbf8f8;"></span>')
    }
	
	if(lan == 'English' && bool == true)
	{
		ctrlName=questions.replace("Ar", "");	
	}
	else if(lan == 'Arabic' && bool == false)
	{
		var ctrlName=questions+'Ar';
	}
	else if(LanChangeController == "Home" && lan == 'English')
	{
		ctrlName='Home';
	}
	else if(LanChangeController == "HomeAr" && lan == 'Arabic')
	{
		ctrlName='HomeAr';
	}
	var mainUrl=location.href;
	if(LanChangeController != "Home" && LanChangeController != "HomeAr")
	{
		urlToLoad=mainUrl.replace(questions, ctrlName);
	}
	else{
		urlToLoad=ctrlName;
	}
    hidePreloader();
	location.href=urlToLoad;
	
}

function rememberMe(email,pwd,chkBoxObj) {
    if ($(chkBoxObj).is(':checked') == true) {
        var loginEmailId = $('#' + email).val();
        var loginPwd = $('#' + pwd).val();
        if (loginEmailId != '' && loginPwd != "") {
            localStorage.setItem('LogedInUserName', loginEmailId);
            localStorage.setItem('LogedInpass', loginPwd);
            localStorage.setItem('rememberChkBox', $(chkBoxObj).val());
        }
        else {
            localStorage.setItem('LogedInUserName', '');
            localStorage.setItem('LogedInpass', '');
            localStorage.setItem('rememberChkBox', '');
        }
    }
    else {
        localStorage.setItem('LogedInUserName', '');
        localStorage.setItem('LogedInpass', '');
        localStorage.setItem('rememberChkBox', '');
    }
}

function pauseVideoClick() {
    $('#video')[0].pause();
    $('#pauseThisVid').css('display', 'none');
    $('#playThisVid').css('display', 'inline-block');
}
function playVideoClick() {
    $('#video')[0].play();
    $('#pauseThisVid').css('display', 'inline-block');
    $('#playThisVid').css('display', 'none');

}

function muteVideo() {
    $('#unmuteThisVid').css('display', 'none');
    $('#muteThisVid').css('display', 'inline-block');
    vid = document.getElementById("video");
    vid.muted = false;
}
function unmuteVideo() {
    $('#unmuteThisVid').css('display', 'inline-block');
    $('#muteThisVid').css('display', 'none');
    vid = document.getElementById("video");
    vid.muted = true;
}
var lat1 = "";
var long1 =""
function getGeoLocation() {
    if ("geolocation" in navigator) {
        navigator.geolocation.getCurrentPosition(function (position) {
            lat1=position.coords.latitude;
            long1=position.coords.longitude;
        });
    } else {
        if (bool == true) {

        }
        else {

        }
        //console.log("Browser doesn't support geolocation!");

    }
}

function calculateDistance(lat1, long1, lat2, long2) {
    var R = 6371; // km
    var dLat = toRad(lat2 - lat1);
    var dLon = toRad(long2 - long1);
    var lat1 = toRad(lat1);
    var lat2 = toRad(lat2);
    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c;
    return d;
}

// Converts numeric degrees to radians
function toRad(Value) {
    return Value * Math.PI / 180;
}

function getToken(credentials, wheres) {
    
    $.ajax({
        url: tempBaseURL+'jwt/gettoken',
        type: 'POST',
        data: JSON.stringify(credentials),
        dataType: 'json',
        contentType: 'application/json',
        success: function (data) {
            var jsonstring = JSON.stringify(data);
            
            '<%Session["MyaccesToken"]= "' + data.access_token + '"; %>';
            
            localStorage.setItem('accessToken', data.access_token);
            
            if(wheres == 'popup') {
                window.location.reload();
            }
        },
        error: function (d) {
        }
    });
}

function mobileNumberValidation(evt, valObj) {
    //var e = evt || window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    else {
        if (($(valObj).val()).length <= 7) {
            return true;
        }
        else {
            return false;
        }
    }    
}

function nameValidation(evt) {
    var keyCode = (evt.which) ? evt.which : evt.keyCode
    if ((keyCode < 65 || keyCode > 90) && (keyCode < 97 || keyCode > 123) && keyCode != 32)
        return false;
    return true;
}

function twelveHourFormat(dateHour) {
  //  console.log(dateHour);
    var timeString = ""+dateHour;
    if (dateHour < 10)
        timeString = "0" + dateHour;
    var H = timeString.split(':')[0];

  // console.log(H);
    var h = (H % 12) || 12;
    var ampm = H < 12 ? " AM" : " PM";
    timeString = h + ":"+timeString.split(':')[1] + ampm;
    return timeString
}

//check if element in view port or not
function inViewportCenter(obj){
    var top_of_element = $(obj).offset().top;
    var bottom_of_element = $(obj).offset().top + $(obj).outerHeight();
    var bottom_of_screen = $(window).scrollTop() + window.innerHeight;
    var top_of_screen = $(window).scrollTop();

    if((bottom_of_screen > top_of_element) && (top_of_screen < bottom_of_element)){ //partially
    //if((top_of_element > top_of_screen) && (bottom_of_element < bottom_of_screen)){ //completely
        return true;
    } else {
        return false;
    }
}