            <div class="clearfix"></div>
            <!--Download App End -->
            <script src="scripts/getArea.js"></script>
            <div class="clearfix"></div>
            <!-- start footer -->
            <footer class="footer-wrapper-area fixedFooter">
                <!-- start footer area -->
                <div class="footer-area-content">
                    <!-- Newsletter -->
                    <div id="footer-newsletter">
                    </div>
                    <!-- END: Newsletter -->
                    <div class="container">
                        <div class="footer-wrapper style-3">
                            <footer class="type-1">
                                <div class="footer-columns-entry">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4">
                                            <h3 class="column-title">Popular cities</h3>
                                            <ul class="column">
                                                <li><a href="#" class="footerATag">Al Asimah</a></li>
                                                <li><a href="#" class="footerATag">Hawalli</a></li>
                                                <li><a href="#" class="footerATag">Kuwait City</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-md-4 col-sm-4">
                                            <h3 class="column-title">Bildarb</h3>
                                            <ul class="column">
                                                <li><a href="#" class="footerATag">About Us</a></li>
                                                <li><a href="#" class="footerATag" href="">Contact Us</a></li>
                                                <li><a href="#" class="footerATag">Terms and conditions</a></li>
                                                <li><a href="" target="_blank" class="footerATag">Bildarb for restaurants</a></li>

                                            </ul>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="col-md-4 col-sm-4">
                                            <h3 class="column-title">Download bildarb app</h3>
                                            <ul class="column">
                                                <li>
                                                    <a target="_blank" href="https://itunes.apple.com/us/app/bildarb/id1349443656?ls=1&amp;mt=8" class="footerATag btn-store btn-appstore">
                                                        <img src="images/app-store.png" style="width:160px" />
                                                    </a>
                                                </li>
                                                <li>
                                                    <a target="_blank" href="https://play.google.com/store/apps/details?id=bildarb.desind.com" class="footerATag ">
                                                        <img src="images/google-play.png" style="width:160px" />
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </footer>
                        </div>
                    </div>
                </div>
                <!-- footer area end -->

                <div class="bottom-footer">

                    <div class="container">

                        <div class="row">

                            <div class="col-sm-4 col-md-4">

                                <p class="copy-right">&#169; Copyright 2018 <span>bildarb</span></p>

                            </div>

                            <div class="col-sm-4 col-md-4">

                            </div>

                            <div class="col-sm-4 col-md-4">
                                <ul class="bottom-footer-menu for-social for_social_icons">
                                    <li><a href="https://twitter.com/bildarb"><i class="ri ri-twitter" data-toggle="tooltip" data-placement="top" title="twitter"></i></a></li>
                                    <li><a href="https://www.facebook.com/bildarb"><i class="ri ri-facebook" data-toggle="tooltip" data-placement="top" title="facebook"></i></a></li>
                                    <li><a href="https://www.linkedin.com/company/bildarb-app"><i class="fa fa-linkedin" data-toggle="tooltip" data-placement="top" title="linkedin"></i></a></li>
                                    <li><a href="https://www.youtube.com/channel/UCUmjBDXm-Yq8vkpUtfjd36A"><i class="ri ri-youtube-play" data-toggle="tooltip" data-placement="top" title="youtube"></i></a></li>
                                    <li><a href="https://www.instagram.com/bildarb/"><i class="fa fa-instagram" data-toggle="tooltip" data-placement="top" title="instagram"></i></a></li>
                                    <li><a href="https://in.pinterest.com/bildarbapp/"><i class="fa fa-pinterest" data-toggle="tooltip" data-placement="top" title="pinterest"></i></a></li>
                                </ul>
                            </div>

                        </div>
                    </div>

                </div>

            </footer>
            <!-- end footer -->

        </div>

    </div>

    <!-- start Main Wrapper -->

    <div>

        <!-- footer area end -->
        <!-- end footer -->
    </div>

    <script>
        function showmsg(type, icon, title, msg) {
            $.notify({
                title: title,
                icon: icon,
                message: msg
            }, {
                type: type,
                //delay: 10000,
                //timer: 1000,
                animate: {
                    enter: 'animated fadeInUp',
                    exit: 'animated fadeOutRight'
                },
                placement: {
                    from: "top",
                    align: "right"
                },

                offset: 20,
                spacing: 10,
                z_index: 1031,
            });
        }

        $(function() {
            localStorage.setItem("lang", "English");
            $("#ref").hover(function() {
                animate(".demo", 'flash');
                return false;
            });
        });

        function animate(element_ID, animation) {
            $(element_ID).addClass(animation);
            var wait = window.setTimeout(function() {
                $(element_ID).removeClass(animation)
            }, 1300);
        }

        function ad() {
            $("#lcp1").click();
        }

        function af() {

            $("#rcp1").click();
        }

        function returnday(number) {
            var weekday = new Array(7);
            weekday[1] = "Monday";
            weekday[2] = "Tuesday";
            weekday[3] = "Wednesday";
            weekday[4] = "Thursday";
            weekday[5] = "Friday";
            weekday[6] = "Saturday";
            weekday[7] = "Sunday";

            return weekday[number];

        }
    </script>

    <!-- JS -->

    <script type="text/javascript" src="scripts/js/jquery-migrate-1.2.1.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>

    <script type="text/javascript" src="scripts/js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="scripts/js/jquery.waypoints.min.js"></script>
    <script type="text/javascript" src="scripts/js/wow.min.js"></script>
    <script>
        new WOW().init();
    </script>
    <script src="scripts/bootstrap.js"></script>
    <script src="scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="scripts/js/jquery.slicknav.min.js"></script>
    <script type="text/javascript" src="scripts/js/jquery.placeholder.min.js"></script>
    <script type="text/javascript" src="scripts/js/bootstrap-tokenfield.js"></script>
    <script type="text/javascript" src="scripts/js/typeahead.bundle.min.js"></script>
    <script type="text/javascript" src="scripts/js/bootstrap-select.min.js"></script>
    <script type="text/javascript" src="scripts/js/jquery-filestyle.min.js"></script>
    <script type="text/javascript" src="scripts/js/bootstrap-select.js"></script>
    <script type="text/javascript" src="scripts/js/ion.rangeSlider.min.js"></script>
    <script type="text/javascript" src="scripts/js/handlebars.min.js"></script>
    <script type="text/javascript" src="scripts/js/jquery.countimator.js"></script>
    <script type="text/javascript" src="scripts/js/jquery.countimator.wheel.js"></script>
    <script type="text/javascript" src="scripts/js/slick.min.js"></script>
    <script type="text/javascript" src="scripts/js/easy-ticker.js"></script>
    <script type="text/javascript" src="scripts/js/jquery.introLoader.min.js"></script>
    <script type="text/javascript" src="scripts/js/jquery.responsivegrid.js"></script>
    <script type="text/javascript" src="scripts/js/customs.js"></script>
    <script src="scripts/bootstrap-notify.min.js"></script>
    <script src="scripts/bootstrap-notify.js"></script>
    <script src="scripts/profile/lg1.js"></script>
    <script src="js/extention/choices.js"></script>
    <script>
        const choices = new Choices('[data-trigger]', {
            searchEnabled: false,
            itemSelectText: '',
        });
    </script>
	<script type="text/javascript">
    var locations = [
      ['Bondi Beach', -33.890542, 151.274856, 4],
      ['Coogee Beach', -33.923036, 151.259052, 5],
      ['Cronulla Beach', -34.028249, 151.157507, 3],
      ['Manly Beach', -33.80010128657071, 151.28747820854187, 2],
      ['Maroubra Beach', -33.950198, 151.259302, 1]
    ];

    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 10,
      center: new google.maps.LatLng(-33.92, 151.25),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) {  
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }
  </script>
</body>

</html>