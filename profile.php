<?php include 'header.php'; ?>
<div class="searchquick3">
	<div class="container">
		<h6 class="logo-content regularweb1">Home &gt; Profile</h6>
		<div class="clearfix"></div>
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 bhoechie-tab-menu">
				  <img src="images/user-icon.png" class="center-block img_profile_pic" />
				  <div class="list-group">
					<a href="#" class="list-group-item active">
					  Profile
					</a>
					<a href="#" class="list-group-item">
					  Change Password
					</a>
					<a href="#" class="list-group-item">
					  Reservation History
					</a>
					<a href="#" class="list-group-item">
					  PIck Up History
					</a>
					<a href="#" class="list-group-item">
					  Favourite Restaurant
					</a>
				  </div>
				</div>
				<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 bhoechie-tab">
					<!-- flight section -->
					<div class="bhoechie-tab-content active">
						<h2 class="logo-content regularweb">Edit Profile</h2>
						<hr class="hrStyleCss">
						<div class="col-md-6">
							<div class="form-group">
							  <label for="usr">Name:</label>
							  <input type="text" class="form-control" value="Ashish Sharma" id="usr">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
							  <label for="usr">Date of Birth:</label>
							  <input type="text" class="form-control" value="22-09-1990" id="usr">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
							  <label for="usr">Email:</label>
							  <input type="text" class="form-control" value="ashish.s.bhl@gmail.com" id="usr">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
							  <label for="usr">Phone Number:</label>
							  <input type="text" class="form-control" value="9672200143" id="usr">
							</div>
						</div>
						<div class="col-md-12">
							<input type="button" class="borderBtn" value="Save">
						</div>
					</div>
					<!-- train section -->
					<div class="bhoechie-tab-content">
						<h2 class="logo-content regularweb">Change Password</h2>
						<hr class="hrStyleCss">
						<div class="col-md-4">
							<div class="form-group">
							  <label for="usr">Old Password:</label>
							  <input type="text" class="form-control" value="Old Password" id="usr">
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
							  <label for="usr">New Password</label>
							  <input type="text" class="form-control" value="New password here.." id="usr">
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
							  <label for="usr">Confirm Password</label>
							  <input type="text" class="form-control" value="New password here.." id="usr">
							</div>
						</div>
						<div class="col-md-12">
							<input type="button" class="borderBtn" value="Change Password">
						</div>
					</div>
		
					<!-- hotel search -->
					<div class="bhoechie-tab-content">
						<h2 class="logo-content regularweb">Reservation History</h2>
						<hr class="hrStyleCss">
						<ul class="nav nav-tabs padding_profile">
							<li class="active"><a data-toggle="tab" href="#home">Upcomming Bookings</a></li>
							<li><a data-toggle="tab" href="#menu1">Previous Bookings</a></li>
						</ul>
						<div class="tab-content tablab padding_profile">
							<div id="home" class="tab-pane fade in active">
								<div class="upcome_booking">
									<h4>No Upcomming Bookings</h4>
								</div>
							</div>
							<div id="menu1" class="tab-pane fade">
								<div class="previus_booking">
									<h4>No Previous Bookings</h4>
								</div>
							</div>
						</div>	
					</div>
					<div class="bhoechie-tab-content">
						<h2 class="logo-content regularweb">Pickup History</h2>
						<hr class="hrStyleCss">
						<div class="pickup_history">
							<h4>No Previous Orders</h4>
						</div>
					</div>
					<div class="bhoechie-tab-content">
						<h2 class="logo-content regularweb">Favourite Restaurant</h2>
						<hr class="hrStyleCss">
						<?php for($i = 0 ; $i < 8 ; $i++) { 
                            include('searchCard.php');
                        } ?>
						
					</div>
				</div>
			</div>
	  </div>
	</div>
</div>
<script>
$(document).ready(function() {
    $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
        e.preventDefault();
        $(this).siblings('a.active').removeClass("active");
        $(this).addClass("active");
        var index = $(this).index();
        $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
        $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
    });
});
</script>
<?php include 'footer.php'; ?>