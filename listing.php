<?php include 'header-with-search.php'; ?>
<div class="pt-40 listing">
    <div class="container">
        <div class="wow bounceInDown">
					<h6 class="logo-content regularweb1">Home > Search</h6>
					<h2 class="logo-content regularweb">Chinese Restaurants</h2>
                    <div class="col-xs-12 col-sm-3 col-md-3">
						<div class="row filter-section">
							<div class="col-md-12 col-sm-12 filter-options">
								<div class="col-md-12">
									<button type="button" class="form-control btn btn-success btndet">Filter</button>
								</div>
								<p class="title boldCss">QUICK FILTERS :</p>
								<div class="col-md-12">
									<label class="control control--checkbox lblCss">Pickup Order
										<input type="checkbox" class="checkBoxCss" id="PickupOrder" >
										<div class="control__indicator"></div>
									</label>
								</div>
								<div class="col-md-12">
									<label class="control control--checkbox lblCss">Preorder
										<input type="checkbox" class="checkBoxCss" id="Preorder">
										<div class="control__indicator"></div>
									</label>
								</div>
								<div class="col-md-12">
									<label class="control control--checkbox lblCss">Has Reservation
										<input type="checkbox" class="checkBoxCss" id="hasReservation">
										<div class="control__indicator"></div>
									</label>
								</div>
								<div class="col-md-12">
									<label class="control control--checkbox lblCss">Open
										<input type="checkbox" class="checkBoxCss" id="Opensss">
										<div class="control__indicator"></div>
									</label>
								</div>
								<div class="col-md-12">
									<label class="control control--checkbox lblCss">Free Reservation
										<input type="checkbox" class="checkBoxCss" id="isFreeReservation">
										<div class="control__indicator"></div>
									</label>
								</div>
								<div class="col-md-12">
									<label class="control control--checkbox lblCss">Seating
										<input type="checkbox" class="checkBoxCss" id="hasSeating">
										<div class="control__indicator"></div>
									</label>
								</div>
								<div class="col-md-12">
									<label class="control control--checkbox lblCss">Featured
										<input type="checkbox" class="checkBoxCss" id="isFeatured">
										<div class="control__indicator"></div>
									</label>
								</div>
								<div class="col-md-12">
									<label class="control control--checkbox lblCss">Active
										<input type="checkbox" class="checkBoxCss" id="isActive">
										<div class="control__indicator"></div>
									</label>
								</div>
								<p class="title boldCss">MORE FILTERS :</p>
								
								<div class="col-md-12">
									<label class="control control--checkbox lblCss">
										Pure Veg
										<input type="checkbox" class="checkBoxCss" id="PureVeg">
										<div class="control__indicator"></div>
									</label>
								</div>
								<div class="col-md-12">
									<label class="control control--checkbox lblCss">
										Buffet
										<input type="checkbox" class="checkBoxCss" id="Buffet">
										<div class="control__indicator"></div>
									</label>
								</div>
								<div class="col-md-12">
									<label class="control control--checkbox lblCss">
										Promotions
										<input type="checkbox" class="checkBoxCss" id="Promotions">
										<div class="control__indicator"></div>
									</label>
								</div>
								<div class="col-md-12">
									<label class="control control--checkbox lblCss">
										Wifi
										<input type="checkbox" class="checkBoxCss" id="Wifi">
										<div class="control__indicator"></div>
									</label>
								</div>
								<div class="col-md-12">
									<label class="control control--checkbox lblCss">
										Pure Vegan
										<input type="checkbox" class="checkBoxCss" id="Vegan">
										<div class="control__indicator"></div>
									</label>
								</div>
								<p class="title boldCss">CUISINES :</p>
								<div class="col-md-12">
									<label class="control control--checkbox lblCss" for="Chinese">Chinese
										<input type="checkbox" class="checkBoxCss" id="Chinese" value="Chinese" alt="29">
										<div class="control__indicator"></div>
									</label>
								</div>
								<div class="col-md-12">
									<label class="control control--checkbox lblCss" for="American">American
										<input type="checkbox" class="checkBoxCss" id="American" value="American" alt="3">
										<div class="control__indicator"></div>
									</label>
								</div>
								<div class="col-md-12">
									<label class="control control--checkbox lblCss" for="Burger">Burger
										<input type="checkbox" class="checkBoxCss" id="Burger" value="Burger" alt="22">
										<div class="control__indicator"></div>
									</label>
								</div>
								<div class="col-md-12">
									<label class="control control--checkbox lblCss" for="HealthyFood">Healthy Food
										<input type="checkbox" class="checkBoxCss" id="HealthyFood" value="Healthy Food" alt="44">
										<div class="control__indicator"></div>
									</label>
								</div>
								<div class="col-md-12">
									<label class="control control--checkbox lblCss" for="Italian">Italian
										<input type="checkbox" class="checkBoxCss" id="Italian" value="Italian" alt="54">
										<div class="control__indicator"></div>
									</label>
								</div>
								<div class="col-md-12">
									<label class="control control--checkbox lblCss" for="Tex-Mex">Tex-Mex
										<input type="checkbox" class="checkBoxCss" id="Tex-Mex" value="Tex-Mex" alt="107">
										<div class="control__indicator"></div>
									</label>
								</div>
								<div class="col-md-12">
									<label class="control control--checkbox lblCss" for="Mexican">Mexican
										<input type="checkbox" class="checkBoxCss" id="Mexican" value="Mexican" alt="69">
										<div class="control__indicator"></div>
									</label>
								</div>
								<div class="col-md-12">
									<label class="control control--checkbox lblCss" for="Mediterranean">Mediterranean
										<input type="checkbox" class="checkBoxCss" id="Mediterranean" value="Mediterranean">
										<div class="control__indicator"></div>
									</label>
								</div>
								<div class="col-md-12">
									<label class="control control--checkbox lblCss" for="MiddleEastern">Middle Eastern
										<input type="checkbox" class="checkBoxCss" id="MiddleEastern" value="Middle Eastern" alt="70">
										<div class="control__indicator"></div>
									</label>
								</div>
								<div class="col-md-12">
									<label class="control control--checkbox lblCss" for="Indian">Indian
										<input type="checkbox" class="checkBoxCss" id="Indian" value="Indian" alt="49">
										<div class="control__indicator"></div>
									</label>
								</div>
								<div class="col-md-12">
									<label class="control control--checkbox lblCss" for="Goan">Goan
										<input type="checkbox" class="checkBoxCss" id="Goan" value="Goan" alt="41">
										<div class="control__indicator"></div>
									</label>
								</div>
								<div class="col-md-12">
									<label class="control control--checkbox lblCss" for="Desserts">Desserts
										<input type="checkbox" class="checkBoxCss" id="Desserts" value="Desserts" alt="32">
										<div class="control__indicator"></div>
									</label>
								</div>
								<div class="col-md-12">
									<label class="control control--checkbox lblCss" for="IceCream">Ice Cream
										<input type="checkbox" class="checkBoxCss" id="IceCream" value="Ice Cream" alt="47">
										<div class="control__indicator"></div>
									</label>
								</div>
								<div class="col-md-12">
									<label class="control control--checkbox lblCss" for="Sandwich">Sandwich
										<input type="checkbox" class="checkBoxCss" id="Sandwich" value="Sandwich" alt="95">
										<div class="control__indicator"></div>
									</label>
								</div>
								<div class="col-md-12">
									<label class="control control--checkbox lblCss" for="International">International
										<input type="checkbox" class="checkBoxCss" id="International" value="International" alt="125">
										<div class="control__indicator"></div>
									</label>
								</div>
								<div class="col-md-12">
									<label class="control control--checkbox lblCss" for="Cafe">Cafe
										<input type="checkbox" class="checkBoxCss" id="Cafe" value="Cafe" alt="24">
										<div class="control__indicator"></div>
									</label>
								</div>
								<div class="col-md-12">
									<label class="control control--checkbox lblCss" for="Rolls">Rolls
										<input type="checkbox" class="checkBoxCss" id="Rolls" value="Rolls" alt="92">
										<div class="control__indicator"></div>
									</label>
								</div>
								<div class="col-md-12">
									<label class="control control--checkbox lblCss" for="Coffee">Coffee
										<input type="checkbox" class="checkBoxCss" id="Coffee" value="Coffee" alt="114">
										<div class="control__indicator"></div>
									</label>
								</div>
								<div class="col-md-12">
									<label class="control control--checkbox lblCss" for="SpecialityCoffee">Speciality Coffee
										<input type="checkbox" class="checkBoxCss" id="SpecialityCoffee" value="Speciality Coffee" alt="115">
										<div class="control__indicator"></div>
									</label>
								</div>
								<div class="col-md-12">
									<label class="control control--checkbox lblCss" for="Japanese">Japanese
										<input type="checkbox" class="checkBoxCss" id="Japanese" value="Japanese" alt="55">
										<div class="control__indicator"></div>
									</label>
								</div>
								<div class="col-md-12">
									<label class="control control--checkbox lblCss" for="StreetFood">Street Food
										<input type="checkbox" class="checkBoxCss" id="StreetFood" value="Street Food" alt="104">
										<div class="control__indicator"></div>
									</label>
								</div>
								<div class="col-md-12">
									<label class="control control--checkbox lblCss" for="FastFood">Fast Food
										<input type="checkbox" class="checkBoxCss" id="FastFood" value="Fast Food" alt="36">
										<div class="control__indicator"></div>
									</label>
								</div>
								<div class="col-md-12">
									<label class="control control--checkbox lblCss" for="Seafood">Seafood
										<input type="checkbox" class="checkBoxCss" id="Seafood" value="Seafood" alt="96">
										<div class="control__indicator"></div>
									</label>
								</div>
								<div class="col-md-12">
									<label class="control control--checkbox lblCss" for="Bakery">Bakery
										<input type="checkbox" class="checkBoxCss" id="Bakery" value="Bakery" alt="11">
										<div class="control__indicator"></div>
									</label>
								</div>
								<div class="col-md-12">
									<label class="control control--checkbox lblCss" for="Asian">Asian
										<input type="checkbox" class="checkBoxCss" id="Asian" value="Asian" alt="7">
										<div class="control__indicator"></div>
									</label>
								</div>
								<div class="col-md-12">
									<label class="control control--checkbox lblCss" for="Sushi">Sushi
										<input type="checkbox" class="checkBoxCss" id="Sushi" value="Sushi" alt="105">
										<div class="control__indicator"></div>
									</label>
								</div>
								<div class="col-md-12">
									<label class="control control--checkbox lblCss" for="Oriental">Oriental
										<input type="checkbox" class="checkBoxCss" id="Oriental" value="Oriental" alt="112">
										<div class="control__indicator"></div>
									</label>
								</div>
								<div class="col-md-12">
									<label class="control control--checkbox lblCss" for="ModernIndian">Modern Indian
										<input type="checkbox" class="checkBoxCss" id="ModernIndian" value="Modern Indian" alt="73">
										<div class="control__indicator"></div>
									</label>
								</div>
								<div class="col-md-12">
									<label class="control control--checkbox lblCss" for="Steak">Steak
										<input type="checkbox" class="checkBoxCss" id="Steak" value="Steak" alt="103">
										<div class="control__indicator"></div>
									</label>
								</div>
								<div class="col-md-12">
									<label class="control control--checkbox lblCss" for="Turkish">Turkish
										<input type="checkbox" class="checkBoxCss" id="Turkish" value="Turkish" alt="110">
										<div class="control__indicator"></div>
									</label>
								</div>
								<div class="col-md-12">
									<label class="control control--checkbox lblCss" for="Korean">Korean
										<input type="checkbox" class="checkBoxCss" id="Korean" value="Korean" alt="61">
										<div class="control__indicator"></div>
									</label>
								</div>
								<div class="col-md-12">
									<label class="control control--checkbox lblCss" for="Beverages">Beverages
										<input type="checkbox" class="checkBoxCss" id="Beverages" value="Beverages" alt="16">
										<div class="control__indicator"></div>
									</label>
								</div>
								<div class="col-md-12">
									<label class="control control--checkbox lblCss" for="Salad">Salad
										<input type="checkbox" class="checkBoxCss" id="Salad" value="Salad" alt="94">
										<div class="control__indicator"></div>
									</label>
								</div>
							</div>
							<div class="col-md-12">
								<button type="button" class="form-control btn btn-success btndet">Filter</button>
							</div>
						</div>
                    </div>
					<div class="col-xs-12 col-sm-9 col-md-9">
						<button id="mapToggle" type="button" class="form-control btn btn-success btndet">View in map</button>
						<div id="map" style="height:400px;display: none;"></div>
						<?php for($i = 0 ; $i < 8 ; $i++) { 
                            include('searchCard.php');
                        } ?>
						<!-- <center><input type="button" class="borderBtn"  value="View all Restaurants" /></center>
						<hr class="hrStyleCss"> -->
                    </div>
                    
        </div>
        
    </div>
</div>
<?php include 'footer.php'; ?>
<script type="text/javascript" src="public/js/custom-js/listing.js"></script>
