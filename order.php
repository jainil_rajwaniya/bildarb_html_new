<?php include 'header.php'; ?>
<body data-spy="scroll" data-target=".navbar" data-offset="50">
	<div class="cartshow carthide"><i class="fa fa-shopping-cart"></i> Cart</div>
	<div class="searchquick3 pad_bot_zero">
	<div class="container">
		<h6 class="logo-content regularweb1">Home &gt; Peacock &gt; Pick up food online from Peacock Restaurant</h6>
		<div class="col-md-8">
			<div class="cardd">
				<p class="elight">Pick up food online from Peacock Restaurant<!--<span class="rating">4.0</span>--></p>
				<h3>Peacock Restaurant</h3>
				<p class="light">Kuwait City, Kuwait <!--•Costs Rs.100.00 for two--></p>
				<div class="col-md-3">
					<p class="text-center">Cuisine Type</p>
					<p class="text-center"><b>Casual Dining, Bar</b></p>
				</div>
				<div class="col-md-3">
					<p class="text-center">Timings</p>
					<p class="text-center"><b>12:00 - 22:30</b></p>
				</div>
				<div class="col-md-3">
					<p class="text-center">Payment Methods</p>
					<p class="text-center"><b>Online</b></p>
				</div>
				<div class="col-md-3">
					<p class="text-center">Phone no.</p>
					<p class="text-center"><b>9910023789</b></p>
				</div>
				<!-- <div class="col-md-3">
					<p class="text-center">Delivery Time</p>
					<p class="text-center"><b>44 mins</b></p>
				</div>
				<div class="col-md-3">
					<p class="text-center">Minimum Order</p>
					<p class="text-center"><b>Rs. 0.00</b></p>
				</div>
				<div class="col-md-3">
					<p class="text-center">Payment Methods</p>
					<p class="text-center"><b>Online</b></p>
				</div>
				<div class="col-md-3">
					<p class="text-center">Recent Order Rating Streak</p>
					<p class="text-center"><span>2</span><span>2</span><span>2</span></p>
				</div> -->
			</div>
			<div class="cardddd carddddq" >
				<nav class="naviscroli">
				  <div class="container-fluid">
					<div>
					  <div class="collapse navbar-collapse" id="myNavbar">
						<div class="scrollmenu">
						<ul class="nav nav-tabs" id="box">
							  <li role="presentation"><a href="#section1" class="bgcolor">Pizzas</a></li>
							  <li role="presentation"><a href="#section2">Burgers</a></li>
							  <li role="presentation"><a href="#section3">Sandwiches</a></li>
							  <li role="presentation"><a href="#section4">Snacks and Chat</a></li>
							  <li role="presentation"><a href="#section5">Namkeen</a></li>
							  <li role="presentation"><a href="#section6">Mawa Sweets</a></li>
							  <li role="presentation"><a href="#section7">Dry Fruit Sweets</a></li>
							  <li role="presentation"><a href="#section8">Desi Ghee Sweets</a></li>
							  <li role="presentation"><a href="#section9">Bengali Sweets</a></li>
							</ul>
						</div>
					  </div>
					</div>
				  </div>
				</nav> 
			</div>
			<div class="cardddd carddddqs">
				<h3>Bestsellers</h3>
				<div class="sweetss" id="section1">
					<h4>Pizzas</h4>
					<div class="itemc">
						<p>Bread Paneer Pakora</p>
						<div class="input-group">
							  <span class="input-group-btn">
								  <button type="button" class="btn btn-danger btn-number"  data-type="minus" data-field="quant[1]">
									<span class="glyphicon glyphicon-minus"></span>
								  </button>
							  </span>
							  <input type="text" name="quant[1]" class="form-control input-number" value="10" min="1" max="100">
							  <span class="input-group-btn">
								  <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="quant[1]">
									  <span class="glyphicon glyphicon-plus"></span>
								  </button>
							  </span>
						  </div>
						  <p class="multirup"><span>Rs. 22.00</span></p>
					</div>
					<div class="itemc">
						<p>Bread Paneer Pakora</p>
						<div class="input-group">
							  <span class="input-group-btn">
								  <button type="button" class="btn btn-danger btn-number"  data-type="minus" data-field="quant[2]">
									<span class="glyphicon glyphicon-minus"></span>
								  </button>
							  </span>
							  <input type="text" name="quant[2]" class="form-control input-number" value="10" min="1" max="100">
							  <span class="input-group-btn">
								  <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="quant[2]">
									  <span class="glyphicon glyphicon-plus"></span>
								  </button>
							  </span>
						  </div>
						  <p class="multirup"><span>Rs. 22.00</span></p>
					</div>
				</div>
				<div class="sweetss" id="section2">
					<h4>Burgers</h4>
					<div class="itemc">
						<p>Bread Paneer Pakora</p>
						<div class="input-group">
							  <span class="input-group-btn">
								  <button type="button" class="btn btn-danger btn-number"  data-type="minus" data-field="quant[3]">
									<span class="glyphicon glyphicon-minus"></span>
								  </button>
							  </span>
							  <input type="text" name="quant[3]" class="form-control input-number" value="10" min="1" max="100">
							  <span class="input-group-btn">
								  <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="quant[3]">
									  <span class="glyphicon glyphicon-plus"></span>
								  </button>
							  </span>
						  </div>
						  <p class="multirup"><span>Rs. 22.00</span></p>
					</div>
					<div class="itemc">
						<p>Bread Paneer Pakora</p>
						<div class="input-group">
							  <span class="input-group-btn">
								  <button type="button" class="btn btn-danger btn-number"  data-type="minus" data-field="quant[4]">
									<span class="glyphicon glyphicon-minus"></span>
								  </button>
							  </span>
							  <input type="text" name="quant[4]" class="form-control input-number" value="10" min="1" max="100">
							  <span class="input-group-btn">
								  <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="quant[4]">
									  <span class="glyphicon glyphicon-plus"></span>
								  </button>
							  </span>
						  </div>
						  <p class="multirup"><span>Rs. 22.00</span></p>
					</div>
				</div>
				<div class="sweetss" id="section3">
					<h4>Sandwiches</h4>
					<div class="itemc">
						<p>Bread Paneer Pakora</p>
						<div class="input-group">
							  <span class="input-group-btn">
								  <button type="button" class="btn btn-danger btn-number"  data-type="minus" data-field="quant[5]">
									<span class="glyphicon glyphicon-minus"></span>
								  </button>
							  </span>
							  <input type="text" name="quant[5]" class="form-control input-number" value="10" min="1" max="100">
							  <span class="input-group-btn">
								  <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="quant[5]">
									  <span class="glyphicon glyphicon-plus"></span>
								  </button>
							  </span>
						  </div>
						  <p class="multirup"><span>Rs. 22.00</span></p>
					</div>
					<div class="itemc">
						<p>Bread Paneer Pakora</p>
						<div class="input-group">
							  <span class="input-group-btn">
								  <button type="button" class="btn btn-danger btn-number"  data-type="minus" data-field="quant[6]">
									<span class="glyphicon glyphicon-minus"></span>
								  </button>
							  </span>
							  <input type="text" name="quant[6]" class="form-control input-number" value="10" min="1" max="100">
							  <span class="input-group-btn">
								  <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="quant[6]">
									  <span class="glyphicon glyphicon-plus"></span>
								  </button>
							  </span>
						  </div>
						  <p class="multirup"><span>Rs. 22.00</span></p>
					</div>
				</div>
				<div class="sweetss" id="section4">
					<h4>Snacks and Chat</h4>
					<div class="itemc">
						<p>Bread Paneer Pakora</p>
						<div class="input-group">
							  <span class="input-group-btn">
								  <button type="button" class="btn btn-danger btn-number"  data-type="minus" data-field="quant[1]">
									<span class="glyphicon glyphicon-minus"></span>
								  </button>
							  </span>
							  <input type="text" name="quant[1]" class="form-control input-number" value="10" min="1" max="100">
							  <span class="input-group-btn">
								  <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="quant[1]">
									  <span class="glyphicon glyphicon-plus"></span>
								  </button>
							  </span>
						  </div>
						  <p class="multirup"><span>Rs. 22.00</span></p>
					</div>
					<div class="itemc">
						<p>Bread Paneer Pakora</p>
						<div class="input-group">
							  <span class="input-group-btn">
								  <button type="button" class="btn btn-danger btn-number"  data-type="minus" data-field="quant[2]">
									<span class="glyphicon glyphicon-minus"></span>
								  </button>
							  </span>
							  <input type="text" name="quant[2]" class="form-control input-number" value="10" min="1" max="100">
							  <span class="input-group-btn">
								  <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="quant[2]">
									  <span class="glyphicon glyphicon-plus"></span>
								  </button>
							  </span>
						  </div>
						  <p class="multirup"><span>Rs. 22.00</span></p>
					</div>
				</div>
				<div class="sweetss" id="section5">
					<h4>Namkeen</h4>
					<div class="itemc">
						<p>Bread Paneer Pakora</p>
						<div class="input-group">
							  <span class="input-group-btn">
								  <button type="button" class="btn btn-danger btn-number"  data-type="minus" data-field="quant[1]">
									<span class="glyphicon glyphicon-minus"></span>
								  </button>
							  </span>
							  <input type="text" name="quant[1]" class="form-control input-number" value="10" min="1" max="100">
							  <span class="input-group-btn">
								  <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="quant[1]">
									  <span class="glyphicon glyphicon-plus"></span>
								  </button>
							  </span>
						  </div>
						  <p class="multirup"><span>Rs. 22.00</span></p>
					</div>
					<div class="itemc">
						<p>Bread Paneer Pakora</p>
						<div class="input-group">
							  <span class="input-group-btn">
								  <button type="button" class="btn btn-danger btn-number"  data-type="minus" data-field="quant[2]">
									<span class="glyphicon glyphicon-minus"></span>
								  </button>
							  </span>
							  <input type="text" name="quant[2]" class="form-control input-number" value="10" min="1" max="100">
							  <span class="input-group-btn">
								  <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="quant[2]">
									  <span class="glyphicon glyphicon-plus"></span>
								  </button>
							  </span>
						  </div>
						  <p class="multirup"><span>Rs. 22.00</span></p>
					</div>
				</div>
				<div class="sweetss" id="section6">
					<h4>Mawa Sweets</h4>
					<div class="itemc">
						<p>Bread Paneer Pakora</p>
						<div class="input-group">
							  <span class="input-group-btn">
								  <button type="button" class="btn btn-danger btn-number"  data-type="minus" data-field="quant[1]">
									<span class="glyphicon glyphicon-minus"></span>
								  </button>
							  </span>
							  <input type="text" name="quant[1]" class="form-control input-number" value="10" min="1" max="100">
							  <span class="input-group-btn">
								  <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="quant[1]">
									  <span class="glyphicon glyphicon-plus"></span>
								  </button>
							  </span>
						  </div>
						  <p class="multirup"><span>Rs. 22.00</span></p>
					</div>
					<div class="itemc">
						<p>Bread Paneer Pakora</p>
						<div class="input-group">
							  <span class="input-group-btn">
								  <button type="button" class="btn btn-danger btn-number"  data-type="minus" data-field="quant[2]">
									<span class="glyphicon glyphicon-minus"></span>
								  </button>
							  </span>
							  <input type="text" name="quant[2]" class="form-control input-number" value="10" min="1" max="100">
							  <span class="input-group-btn">
								  <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="quant[2]">
									  <span class="glyphicon glyphicon-plus"></span>
								  </button>
							  </span>
						  </div>
						  <p class="multirup"><span>Rs. 22.00</span></p>
					</div>
				</div>
				<div class="sweetss" id="section7">
					<h4>Dry Fruit Sweets</h4>
					<div class="itemc">
						<p>Bread Paneer Pakora</p>
						<div class="input-group">
							  <span class="input-group-btn">
								  <button type="button" class="btn btn-danger btn-number"  data-type="minus" data-field="quant[1]">
									<span class="glyphicon glyphicon-minus"></span>
								  </button>
							  </span>
							  <input type="text" name="quant[1]" class="form-control input-number" value="10" min="1" max="100">
							  <span class="input-group-btn">
								  <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="quant[1]">
									  <span class="glyphicon glyphicon-plus"></span>
								  </button>
							  </span>
						  </div>
						  <p class="multirup"><span>Rs. 22.00</span></p>
					</div>
					<div class="itemc">
						<p>Bread Paneer Pakora</p>
						<div class="input-group">
							  <span class="input-group-btn">
								  <button type="button" class="btn btn-danger btn-number"  data-type="minus" data-field="quant[2]">
									<span class="glyphicon glyphicon-minus"></span>
								  </button>
							  </span>
							  <input type="text" name="quant[2]" class="form-control input-number" value="10" min="1" max="100">
							  <span class="input-group-btn">
								  <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="quant[2]">
									  <span class="glyphicon glyphicon-plus"></span>
								  </button>
							  </span>
						  </div>
						  <p class="multirup"><span>Rs. 22.00</span></p>
					</div>
				</div>
				<div class="sweetss" id="section8">
					<h4>Desi Ghee Sweets</h4>
					<div class="itemc">
						<p>Bread Paneer Pakora</p>
						<div class="input-group">
							  <span class="input-group-btn">
								  <button type="button" class="btn btn-danger btn-number"  data-type="minus" data-field="quant[1]">
									<span class="glyphicon glyphicon-minus"></span>
								  </button>
							  </span>
							  <input type="text" name="quant[1]" class="form-control input-number" value="10" min="1" max="100">
							  <span class="input-group-btn">
								  <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="quant[1]">
									  <span class="glyphicon glyphicon-plus"></span>
								  </button>
							  </span>
						  </div>
						  <p class="multirup"><span>Rs. 22.00</span></p>
					</div>
					<div class="itemc">
						<p>Bread Paneer Pakora</p>
						<div class="input-group">
							  <span class="input-group-btn">
								  <button type="button" class="btn btn-danger btn-number"  data-type="minus" data-field="quant[2]">
									<span class="glyphicon glyphicon-minus"></span>
								  </button>
							  </span>
							  <input type="text" name="quant[2]" class="form-control input-number" value="10" min="1" max="100">
							  <span class="input-group-btn">
								  <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="quant[2]">
									  <span class="glyphicon glyphicon-plus"></span>
								  </button>
							  </span>
						  </div>
						  <p class="multirup"><span>Rs. 22.00</span></p>
					</div>
				</div>
				<div class="sweetss" id="section9">
					<h4>Bengali Sweets</h4>
					<div class="itemc">
						<p>Bread Paneer Pakora</p>
						<div class="input-group">
							  <span class="input-group-btn">
								  <button type="button" class="btn btn-danger btn-number"  data-type="minus" data-field="quant[1]">
									<span class="glyphicon glyphicon-minus"></span>
								  </button>
							  </span>
							  <input type="text" name="quant[1]" class="form-control input-number" value="10" min="1" max="100">
							  <span class="input-group-btn">
								  <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="quant[1]">
									  <span class="glyphicon glyphicon-plus"></span>
								  </button>
							  </span>
						  </div>
						  <p class="multirup"><span>Rs. 22.00</span></p>
					</div>
					<div class="itemc">
						<p>Bread Paneer Pakora</p>
						<div class="input-group">
							  <span class="input-group-btn">
								  <button type="button" class="btn btn-danger btn-number"  data-type="minus" data-field="quant[2]">
									<span class="glyphicon glyphicon-minus"></span>
								  </button>
							  </span>
							  <input type="text" name="quant[2]" class="form-control input-number" value="10" min="1" max="100">
							  <span class="input-group-btn">
								  <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="quant[2]">
									  <span class="glyphicon glyphicon-plus"></span>
								  </button>
							  </span>
						  </div>
						  <p class="multirup"><span>Rs. 22.00</span></p>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4" id="cart_add">
			<!-- <div class="cardd">
				<h4>Delivery Location</h4>
				<input type="text" class="form-control datepic" placeholder="Enter your delivery area...">
				<button type="button" class="form-control btn btn-success btndet">Detect Location</button>
			</div> -->
			<div class="carddx1">
				<img src="images/empty-cart.png" />
				<h4 class="text-center">Cart is empty</h4>
			</div>
			<div class="carddd cartitemcard">
				<h4>Your Order</h4>
				<div class="scrollorder">
					<div class="itemc">
						<p>Bread Paneer Pakora</p>
						<div class="input-group">
							  <span class="input-group-btn">
								  <button type="button" class="btn btn-danger btn-number"  data-type="minus" data-field="quant[1]">
									<span class="glyphicon glyphicon-minus"></span>
								  </button>
							  </span>
							  <input type="text" name="quant[1]" class="form-control input-number" value="10" min="1" max="100">
							  <span class="input-group-btn">
								  <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="quant[1]">
									  <span class="glyphicon glyphicon-plus"></span>
								  </button>
							  </span><span class="multiply">x</span>
						  </div>
						  <p class="multirup"><span>Rs. 22.00</span> <span>Rs. 22.00</span></p>
					</div>
					<div class="itemc">
						<p>Bread Paneer Pakora</p>
						<div class="input-group">
							  <span class="input-group-btn">
								  <button type="button" class="btn btn-danger btn-number"  data-type="minus" data-field="quant[2]">
									<span class="glyphicon glyphicon-minus"></span>
								  </button>
							  </span>
							  <input type="text" name="quant[2]" class="form-control input-number" value="10" min="1" max="100">
							  <span class="input-group-btn">
								  <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="quant[2]">
									  <span class="glyphicon glyphicon-plus"></span>
								  </button>
							  </span><span class="multiply">x</span>
						  </div>
						  <p class="multirup"><span>Rs. 22.00</span> <span>Rs. 22.00</span></p>
					</div>
					<div class="itemc">
						<p>Bread Paneer Pakora</p>
						<div class="input-group">
							  <span class="input-group-btn">
								  <button type="button" class="btn btn-danger btn-number"  data-type="minus" data-field="quant[3]">
									<span class="glyphicon glyphicon-minus"></span>
								  </button>
							  </span>
							  <input type="text" name="quant[3]" class="form-control input-number" value="10" min="1" max="100">
							  <span class="input-group-btn">
								  <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="quant[3]">
									  <span class="glyphicon glyphicon-plus"></span>
								  </button>
							  </span><span class="multiply">x</span>
						  </div>
						  <p class="multirup"><span>Rs. 22.00</span> <span>Rs. 22.00</span></p>
					</div>
					<div class="itemc">
						<p>Bread Paneer Pakora</p>
						<div class="input-group">
							  <span class="input-group-btn">
								  <button type="button" class="btn btn-danger btn-number"  data-type="minus" data-field="quant[4]">
									<span class="glyphicon glyphicon-minus"></span>
								  </button>
							  </span>
							  <input type="text" name="quant[4]" class="form-control input-number" value="10" min="1" max="100">
							  <span class="input-group-btn">
								  <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="quant[4]">
									  <span class="glyphicon glyphicon-plus"></span>
								  </button>
							  </span><span class="multiply">x</span>
						  </div>
						  <p class="multirup"><span>Rs. 22.00</span> <span>Rs. 22.00</span></p>
					</div>
					<div class="itemc">
						<p>Bread Paneer Pakora</p>
						<div class="input-group">
							  <span class="input-group-btn">
								  <button type="button" class="btn btn-danger btn-number"  data-type="minus" data-field="quant[5]">
									<span class="glyphicon glyphicon-minus"></span>
								  </button>
							  </span>
							  <input type="text" name="quant[5]" class="form-control input-number" value="10" min="1" max="100">
							  <span class="input-group-btn">
								  <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="quant[5]">
									  <span class="glyphicon glyphicon-plus"></span>
								  </button>
							  </span><span class="multiply">x</span>
						  </div>
						  <p class="multirup"><span>Rs. 22.00</span> <span>Rs. 22.00</span></p>
					</div>
					<div class="itemc">
						<p>Bread Paneer Pakora</p>
						<div class="input-group">
							  <span class="input-group-btn">
								  <button type="button" class="btn btn-danger btn-number"  data-type="minus" data-field="quant[6]">
									<span class="glyphicon glyphicon-minus"></span>
								  </button>
							  </span>
							  <input type="text" name="quant[6]" class="form-control input-number" value="10" min="1" max="100">
							  <span class="input-group-btn">
								  <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="quant[6]">
									  <span class="glyphicon glyphicon-plus"></span>
								  </button>
							  </span><span class="multiply">x</span>
						  </div>
						  <p class="multirup"><span>Rs. 22.00</span> <span>Rs. 22.00</span></p>
					</div>
				</div>
			</div>
			<div class="side-bar-bottom">
				<p class="multirup"><span>Subtotal</span> <span>Rs. 290</span></p>
				<p><span>(plus taxes)</span></p>
				<button type="button" class="form-control btn btn-success btndet">Continue</button>
			</div>
		</div>
	</div>
</div>
<?php include 'footer.php'; ?>
<script>
$('.btn-number').click(function(e){
    e.preventDefault();
    
    fieldName = $(this).attr('data-field');
    type      = $(this).attr('data-type');
    var input = $("input[name='"+fieldName+"']");
    var currentVal = parseInt(input.val());
    if (!isNaN(currentVal)) {
        if(type == 'minus') {
            
            if(currentVal > input.attr('min')) {
                input.val(currentVal - 1).change();
            } 
            if(parseInt(input.val()) == input.attr('min')) {
                $(this).attr('disabled', true);
            }

        } else if(type == 'plus') {

            if(currentVal < input.attr('max')) {
                input.val(currentVal + 1).change();
            }
            if(parseInt(input.val()) == input.attr('max')) {
                $(this).attr('disabled', true);
            }

        }
    } else {
        input.val(0);
    }
});
$('.input-number').focusin(function(){
   $(this).data('oldValue', $(this).val());
});
$('.input-number').change(function() {
    
    minValue =  parseInt($(this).attr('min'));
    maxValue =  parseInt($(this).attr('max'));
    valueCurrent = parseInt($(this).val());
    
    name = $(this).attr('name');
    if(valueCurrent >= minValue) {
        $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the minimum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    if(valueCurrent <= maxValue) {
        $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the maximum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    
    
});
$(".input-number").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
</script>


<script>
$('a').click(function(){
	$('html, body').animate({
        scrollTop: $( $(this).attr('href') ).offset().top
    }, 500);
    return false;
});

</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>
 $(document).ready(function() {
     $(document).scroll(function() {
         //get document scroll position
         var position = $(document).scrollTop();
         //get header height
         var header = $('nav').outerHeight();

         //check active section
         $('.sweetss').each(function(i) {
             if ($(this).position().top <= (position + header)) {
                 $(".naviscroli .scrollmenu li a.bgcolor").removeClass('bgcolor');
                 $(".naviscroli .scrollmenu li a").eq(i).addClass('bgcolor');
             }
         });
     });
     $(".side-bar-bottom").hide();
     $(".carddd").hide();
     $(window).scroll(function() {
         if ($(this).scrollTop() > 220) { // this refers to window
             $(".carddd").addClass("cardfixx");
             $(".carddddq").addClass("cardfixx1");
             //$(".carddddqs > div.firstpad").addClass("addpad");
             $(".carddd").show();
             $(".side-bar-bottom").show();
			 $("#header").hide();	 
         } else if ($(this).scrollTop() < 220) { // this refers to window
		 $(".cartshow").removeClass("cartshow_true");
             $(".carddd").removeClass("cardfixx");
             $(".carddddq").removeClass("cardfixx1");
             //$(".carddddqs > div.firstpad").removeClass("addpad");
             $(".carddd").hide();
             $(".side-bar-bottom").hide();
			 $("#header").show();			 
         }
     });
     var lastScrollTop = 0;
     x = 150;
     $(window).scroll(function(event) {
         var st = $(this).scrollTop();
         if (st > lastScrollTop) {
             $(".naviscroli .scrollmenu > .nav > li").css("transform", "translateX(" + (x -= 2) + "px)");
			 $(".cartshow").addClass("cartshow_true");
             
         } else {
             $(".naviscroli .scrollmenu > .nav > li").css("transform", "translateX(" + (x += 1) + "px)");
         }
         lastScrollTop = st;
     });
     $(".naviscroli .scrollmenu li a").click(function() {
		 $("a").removeClass("bgcolor");
         var hrefval1 = $(this);
         var hrefval = $(this).attr("href");
         var itemId = hrefval.substring(1, hrefval.length);
         if (itemId) {
             $(hrefval1).addClass("bgcolor");
         }

     });
	 $(".cartshow").click(function(){
		$("#cart_add .carddd").toggleClass("cardfixx12");
		$("#cart_add .side-bar-bottom").toggleClass("cardfixx12");
	 });
 });
</script>
<script>
jQuery(function ($) {
    $.fn.hScroll = function (amount) {
        amount = amount || 150;
        $(this).bind("DOMMouseScroll mousewheel", function (event) {
            var oEvent = event.originalEvent, 
                direction = oEvent.detail ? oEvent.detail * -amount : oEvent.wheelDelta, 
                position = $(this).scrollLeft();
            position += direction > 0 ? -amount : amount;
            $(this).scrollLeft(position);
            event.preventDefault();
        })
    };
});

$(document).ready(function() {
    $('#box').hScroll(20); // You can pass (optionally) scrolling amount
});
</script>
