<html>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<head>

    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="css/bootstrap.css" rel="stylesheet" />
    <link rel="shortcut icon" href="images/favicon.ico" />
    <title style="font:bold">Bildarb</title>
    <link href="content/assets/icon.css" rel="stylesheet" />

    <link href="content/css/main.css" rel="stylesheet">
    <link href="content/bootstrap.css" rel="stylesheet" />
    <link href="content/bootstrap.min.css" rel="stylesheet" />

    <script type="text/javascript" src="scripts/js/jquery-1.11.3.min.js"></script>

    <!-- CSS Font Icons -->
    <!----Loader jquery-->

    <link rel="stylesheet" href="icons/linearicons/style.css">
    <link rel="stylesheet" href="icons/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="icons/simple-line-icons/css/simple-line-icons.css">
    <link rel="stylesheet" href="icons/ionicons/css/ionicons.css">
    <link rel="stylesheet" href="icons/pe-icon-7-stroke/css/pe-icon-7-stroke.css">
    <link rel="stylesheet" href="icons/rivolicons/style.css">
    <link rel="stylesheet" href="icons/flaticon-line-icon-set/flaticon-line-icon-set.css">
    <link rel="stylesheet" href="icons/flaticon-streamline-outline/flaticon-streamline-outline.css">
    <link rel="stylesheet" href="icons/flaticon-thick-icons/flaticon-thick.css">
    <link rel="stylesheet" href="icons/flaticon-ventures/flaticon-ventures.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.4/sweetalert2.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.4/sweetalert2.js"></script>
    <script src="scripts/common.js"></script>
    <script defer src="https://cdn.rawgit.com/chrisveness/geodesy/v1.1.2/latlon-spherical.js"></script>
    <script defer src="https://cdn.rawgit.com/chrisveness/geodesy/v1.1.2/dms.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDOTK1eZJnW0uUoV-ibeT810Z2EfSL3PBs" async defer></script>
    <!-- CSS Custom -->
    <link href="content/css/style.css" rel="stylesheet">
</head>

<body class="not-transparent-header">
    <div id="mybannerApp"></div>
    <div class="container-wrapper">
        <!-- start Header -->
        <header id="header">
            <div class="navbar navbar-default navbar-fixed-top">
                <div class="topnavbar">
                    <ul class="navrightl">
                        <li><a href=""><i class="fa fa-mobile"></i> Get The <span>App</span></a></li>
                    </ul>
                    <ul class="navrightb">
                        <li class="borright"><a href="#" data-toggle="modal" data-target="#myModal22" onclick="ad()">Login</a></li>
                        <li><a href="#" data-toggle="modal" data-target="#myModal22" onclick="af()">Register</a></li>
                        <li class="safariBrowserLanDd">
                            <div class="dropdown">
                                <button class="btn btn-default dropdown-toggle langDd safariBrowserlangDd language_dropdown_btn" type="button" id="langDd" data-toggle="dropdown">EN<span class="caret"></span></button>
                                <ul class="dropdown-menu language_dropdown" role="menu" aria-labelledby="menu1">
                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="" class="language_">English</a></li>
                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="" onclick="" class="language_">عربي</a></li>
                                </ul>
                            </div>
                        </li>

                    </ul>
                </div>
                <div class="container">
                    <div class="navbar-header">
                        <button button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a href="index.php"><img class="logoWidth" src="images/logo.png" alt="Logo" /></a>
                    </div>
                    <div id="navbar" class="collapse navbar-collapse navbar-responsive-collapse">
                        <ul class="nav navbar-nav menuCenterClass">
                            <li>
                                <a href="index.php"><font style="text-transform:uppercase">H</font>ome</a>
                            </li>
                            <li>
                                <a href="about.php"><font style="text-transform:uppercase">A</font>bout</a>
                            </li>
                            <li>
                                <a href="#"><font class="uppercase">A</font>dd restaurant</a>
                            </li>
                        </ul>

                    </div>
                </div>
            </div>
        </header>