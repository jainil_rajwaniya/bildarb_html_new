<html>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>

    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="css/bootstrap.css" rel="stylesheet" />
    <link rel="shortcut icon" href="images/favicon.ico" />
    <title style="font:bold">Bildarb</title>
    <link href="content/assets/icon.css" rel="stylesheet" />
    
    
    
    
    <link href="content/css/main.css" rel="stylesheet">
    <link href="content/bootstrap.css" rel="stylesheet" />
    <link href="content/bootstrap.min.css" rel="stylesheet" />
    
    <script type="text/javascript" src="scripts/js/jquery-1.11.3.min.js"></script>
   
<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
    <!-- CSS Font Icons -->
    <!----Loader jquery-->
    

    

    <link rel="stylesheet" href="icons/linearicons/style.css">
    <link rel="stylesheet" href="icons/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="icons/simple-line-icons/css/simple-line-icons.css">
    <link rel="stylesheet" href="icons/ionicons/css/ionicons.css">
    <link rel="stylesheet" href="icons/pe-icon-7-stroke/css/pe-icon-7-stroke.css">
    <link rel="stylesheet" href="icons/rivolicons/style.css">
    <link rel="stylesheet" href="icons/flaticon-line-icon-set/flaticon-line-icon-set.css">
    <link rel="stylesheet" href="icons/flaticon-streamline-outline/flaticon-streamline-outline.css">
    <link rel="stylesheet" href="icons/flaticon-thick-icons/flaticon-thick.css">
    <link rel="stylesheet" href="icons/flaticon-ventures/flaticon-ventures.css">
    <link rel="stylesheet" href="../cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.4/sweetalert2.css">
    <script src="../cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.4/sweetalert2.js"></script>
    <script src="scripts/common.js"></script>
    <!-- CSS Custom -->
    <link href="content/css/style.css" rel="stylesheet">
    



    <style>
        a:hover, a:focus {
            color: #fc204e;
            text-decoration: none !important;
        }

        .btn-primary:hover, .btn-primary:focus, .btn-primary:active, .btn-primary.active, .open .dropdown-toggle.btn-primary {
            color: #fff;
            background-color: #fc204e !important;
        }

        .btn-primary:hover, .btn-primary:focus, .btn-primary:active, .btn-primary.active, .open .dropdown-toggle.btn-primary {
            color: #fff;
            background-color: #fc204e;
            border-color: #fc204e;
        }
#map{position: relative;
overflow: hidden;
width: 98.4%;
border: 2px solid #fff;
margin-bottom: 12px;
margin-left: 14px;}
        .btn-image:before {
            content: "";
            width: 22px;
            margin-bottom: -2px;
            height: 18px;
            display: inline-block;
            /* margin-right: 5px; */
            /* vertical-align: text-top; */
            background-color: transparent;
            /* background-position: center center; */
            background-repeat: no-repeat;
        }

        .btnPickup,
        .btnReservation, .btnPreorder {
            padding: 5px 10px 6px 10px !important;
            margin-top: 5px !important;
        }

            .btnPickup:before {
                background-image: url(images/icns/pickup1.png);
                background-size: 90% 90%;
                background-position-y: 2px;
            }

            .btnPreorder:before {
                background-image: url(images/icns/preorder.png);
                background-size: 90% 90%;
                background-position-y: 2px;
            }

            .btnReservation:before {
                background-image: url(images/icns/reservation1.png);
                background-size: 91% 95%;
                background-position-y: 3px;
            }

        .footerATag {
            font-size: 15px !important;
        }

        .rcross:after {
            font-weight: bold;
            font-size: larger;
            content: '';
            margin-top: -14px;
            margin-right: -23px;
            margin-left: 542px;
            display: block;
            height: 20px;
            z-index: 999999;
            width: 20px;
            position: absolute;
            background: url(images/cross.png) no-repeat 0px 0px !important;
        }

        .txtBoxcss {
            padding: 12px 4px !important;
        }

        #RestaurantRequestForm {
            padding-top: 36px;
        }

        /*#branch-banner .branch-banner-close {
            display: inline-block;
            float: left;
            width: 24px;
            padding: 30px 0;
            text-align: center;
            color: grey;
            font-size: 20px;
            position: relative;
            top: 50%;
            transform: translateY(-50%);
            -webkit-transform: translateY(-50%);
            -ms-transform: translateY(-50%);
            z-index: 2;
        }

        #branch-banner .branch-banner-icon {
            float: left;
            border-radius: 10px;
            width: 63px;
            height: 63px;
            margin-right: 10px;
            padding: 10px 0;
            position: relative;
            top: 35%;
            transform: translateY(-50%);
            -webkit-transform: translateY(-50%);
            -ms-transform: translateY(-50%);
            z-index: 2;
        }

            #branch-banner .branch-banner-icon img {
                width: 63px;
                height: 63px;
                border-radius: 10px;
            }

        #branch-banner .branch-banner-title {
            font-weight: bold;
            color: #333333;
            padding-top: 5px;
            padding-bottom: 1px;
            font-size: 12px;
        }

        #branch-banner .branch-banner-description {
            overflow: hidden;
            color: #333333;
            font-size: 11px;
        }

        #branch-banner .branch-banner-button {
            font-size: 12px;
            border: 1px solid;
            border-radius: 10px;
            padding: 10px 10px;
            text-decoration: none;
            color: #1CADCE;
            font-weight: bold;
            text-align: center;
            letter-spacing: 1.15px;
            display: inline-block;
            vertical-align: middle;
            margin-right: 10px;
            position: relative;
            z-index: 2;
        }

        #branch-banner .branch-banner-stars {
            display: inline-block;
            color: rgba(255, 180, 0, 1);
            letter-spacing: -2px;
            font-size: 16px;
            margin-left: -2px;
        }

        #branch-banner .branch-banner-reviews {
            font-size: 12px;
            color: #777;
            display: inline-block;
            margin-left: 4px;
            position: relative;
            top: -1px;
        }

        #branch-banner .branch-banner-stars span {
            position: relative;
        }

        #branch-banner .branch-banner-content {
            width: 100%;
            overflow: hidden;
            height: 67px;
            background: white;
            color: #333;
            border-top: 1px solid #ddd;
        }

        #branch-banner .branch-banner-left {
            height: 100%;
            background-image: url(.../../images/downloadbanner.png);
            background-repeat: no-repeat;
            background-size: cover;
        }

        #branch-banner .branch-banner-details {
            position: relative;
            top: 50%;
            transform: translateY(-50%);
            -webkit-transform: translateY(-50%);
            -ms-transform: translateY(-50%);
        }

        #branch-banner .branch-banner-right {
            float: right;
            height: 100%;
        }

            #branch-banner .branch-banner-right:before {
                display: inline-block;
                vertical-align: middle;
                content: "";
                height: 100%;
            }

        #branch-banner {
            -webkit-text-size-adjust: 100%;
            font-family: 'Open Sans', sans-serif;
            width: 100%;
            z-index: 99999;
            -webkit-font-smoothing: antialiased;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
            -webkit-transition: all 0.25s ease;
            transition: all 00.25s ease;
           
        }

        .bannerBtnAndroid {
            height: 21px;
            right: 0px;
            position: absolute;
            top: 33px;
            width: 65px;
        }

        .bannerBtniOS {
            height: 21px;
            right: 80px;
            position: absolute;
            top: 33px;
            width: 70px;
        }*/
    </style>
</head>

<body class="not-transparent-header">
    <div id="mybannerApp"></div>
    <div class="modal fade" id="modalIntroPop" tabindex="-1" role="dialog" aria-labelledby="smallModal" aria-hidden="true">
        <div class="modal-dialog modal-sm" style="left:0;">
            <div class="modal-content" style="border-radius: 5px !important;top:40px;">
                <div class="modal-body" style="padding:0 !important">
                    <div class="modal-content" style="backface-visibility:hidden;background-color: #0c222d;">
                        <i class="rcross" name="" data-dismiss="modal" aria-hidden="true" style="cursor:pointer;"></i>

                        <div class="modal-body">

                            <div style="width:100%;text-align:center">
                                <img src="images/placeholder1.png" style="width:190px;display:block; margin:auto;" />
                            </div>
                            <h3 class="" style="color:white;text-align: center;font-size:14px">
                                Taking Table Reservation <br /> & Pickup a Step further
                                <br />
                            </h3>


                            <h3 style=" color: white;  text-align: center; ">
                                <span style="color:#fc204e; font-weight: bold;font-size: 24px;">
                                    Download Now
                                </span>

                            </h3>
                            <h3 style=" color: white;  text-align: center; ">
                                <span style="font-size: 14px;">Click on the Icons Below</span>
                            </h3>
                            <div class="row">
                                <div class="col-sm-6" style="margin-bottom: 10px;"><a target="_blank" href="https://itunes.apple.com/us/app/bildarb/id1349443656?ls=1&amp;mt=8" class=""><img src="images/app-store.png" style="width: 150px;display:block;margin:auto" /></a></div>
                                <div class="col-sm-6"><a target="_blank" href="https://play.google.com/store/apps/details?id=bildarb.desind.com" class=""><img src="images/google-play.png" style="width: 150px;display:block;margin:auto" /></a></div>
                            </div>
                            <div class="row">
                            </div>
                        </div>
                        <div class="modal-footer" style="border:0px !important;padding:0px !important;margin:0px !important;">
                            <div class="col-sm-12 col-md-12">
                                <ul class="bottom-footer-menu for-social" style="margin: 5% 15%;float: none;display:block;text-align:center">
                                    <li><a target="_blank" href="https://www.instagram.com/bildarb/" style="color:#fff;"><i class="fa fa-instagram" data-toggle="tooltip" data-placement="top" title="instagram"></i></a></li>
                                    <li><a target="_blank" href="https://twitter.com/bildarb" style="color:#fff;"><i class="ri ri-twitter" data-toggle="tooltip" data-placement="top" title="twitter"></i></a></li>
                                    <li><a target="_blank" href="https://www.facebook.com/bildarb" style="color:#fff;"><i class="ri ri-facebook" data-toggle="tooltip" data-placement="top" title="facebook"></i></a></li>*
                                    <li><a target="_blank" href="https://www.youtube.com/channel/UCUmjBDXm-Yq8vkpUtfjd36A" style="color:#fff;"><i class="ri ri-youtube-play" data-toggle="tooltip" data-placement="top" title="youtube"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div id="preloader" style="display: none;">
        <div id="loading-animation" style="display: block;">&nbsp;</div>
    </div>
    <div class="modal fade" id="addRestaurant" tabindex="-1" role="dialog" aria-labelledby="smallModal" aria-hidden="true">
        <div class="modal-dialog modal-sm" style="left:0;">
            <div class="modal-content" style="border-radius: 5px !important;top:40px;">
                <div class="modal-body" style="padding:0 !important">
                    <div class="modal-content" style="backface-visibility:hidden;">
                        <div class="image-before" style="margin-top:-1px;"></div>
                        <i class="rcross" name="" data-dismiss="modal" aria-hidden="true" style="cursor:pointer;"></i>

                        <div class="modal-body">
                            <h2 style="text-align:center;">Add Restaurant in to Bildarb</h2>
                            <form method="post" id="RestaurantRequestForm" name="RestaurantRequestForm">
                                <input type="text" id="restaurantName" name="restaurantName" placeholder="Name of Restaurant" class="txtBoxcss" style="" />
                                <input type="text" id="ownerName" name="ownerName"  placeholder="Name of owner" class="txtBoxcss" style="" />
                                <input type="text" id="establishYear" name="establishYear" placeholder="Establishment year" class="txtBoxcss" style="" />
                                <input type="text" id="cuisineService" name="cuisineService" placeholder="Service cuisine" class="txtBoxcss" style="" />
                                <input type="email" id="email" name="email" placeholder="Email id" class="txtBoxcss" style="" />
                                <input type="text" id="contactNumber" name="contactNumber" placeholder="Contact number" class="txtBoxcss" style="" />
                                <textarea class="txtBoxcss" placeholder="Address" id="restaurantAddress" id="restaurantAddress" name="restaurantAddress"></textarea>
                                <div style="margin-top:40px;">
                                    <center>
                                        <input type="button" class="btn-primary borderRadius pinkBtncss" style="padding: 6px;font-size: 14px;display: inline-block;width:150px;" value="Add" />
                                        <input type="button" class="btn-primary borderRadius pinkBtncss" style="padding: 6px;font-size: 14px;display: inline-block;width:150px;" value="Cancel" />
                                    </center>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer" style="border:0px !important;padding:0px !important;margin:0px !important;"></div>
                    </div>

                </div>
            </div>
        </div>
    </div>



    <div class="modal fade" id="contactUs" tabindex="-1" role="dialog" aria-labelledby="smallModal" aria-hidden="true">
        <div class="modal-dialog modal-sm" style="left:0;">
            <div class="modal-content" style="border-radius: 5px !important;top:40px;">
                <div class="modal-body" style="padding:0 !important">
                    <div class="modal-content" style="backface-visibility:hidden;">
                        <div class="image-before" style="margin-top:-1px;"></div>
                        <i class="rcross" name="" data-dismiss="modal" aria-hidden="true" style="cursor:pointer;"></i>

                        <div style="margin-top:20px;"> <center>  <h2 class="modal-title">Contact Us</h2> </center> </div>

                        <div class="modal-body">
                            <form method="post" id="contactUsForm" name="contactUsForm">
                                <input type="text" id="firstName" placeholder="First Name"  class="txtBoxcss" style="" />
                                <input type="text" id="lastName" placeholder="Last Name"  class="txtBoxcss" style="" />
                                <input type="email" id="conEmail" placeholder="Email" class="txtBoxcss" style="" />
                                <input type="text" id="conPhoneNo" placeholder="Phone No." class="txtBoxcss" style="" />
                                <textarea class="txtBoxcss" placeholder="Message" id="conAddress"></textarea>
                                <div style="margin-top:40px;">
                                    <center>
                                        <input type="button" class="btn-primary borderRadius pinkBtncss" style="padding: 6px;font-size: 14px;display: inline-block;width:150px;" value="Send"/>
                                        <input type="button" class="btn-primary borderRadius pinkBtncss" style="padding: 6px;font-size: 14px;display: inline-block;width:150px;" value="Cancel" />
                                    </center>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer" style="border:0px !important;padding:0px !important;margin:0px !important;"></div>
                    </div>

                </div>
            </div>
        </div>
    </div>



   
    











                            <div class="modal fade" id="myModal22" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" style="top:10%">
                                <div class="modal-dialog" style="left:64px !important;">
                                    <div class="modal-content" style="background: transparent;backface-visibility:hidden;width:67%">
                                        
                                        <div class="modal-body" style="padding:0">
                                            <div class="login-container">
                                                <!-- Combined Form Content -->
                                                <div class="login-container-content">
                                                    <ul class="nav nav-tabs nav-justified" style="margin-left:0px !important;">
                                                        <li class="active link-one"><a href="#login-block2" data-toggle="tab" id="lcp1"><i class="fa fa-sign-in"></i>Sign In</a></li>
                                                        <li class="link-two"><a href="#register-block2" data-toggle="tab" id="rcp1"><i class="fa fa-pencil"></i>Sign Up</a></li>
                                                        <li class="link-three"><a href="#contact-block2" data-toggle="tab"><i class="fa fa-envelope"></i>Contact</a></li>
                                                    </ul>
                                                    <div class="tab-content">
                                                        <div class="tab-pane active fade in" id="login-block2">
                                                            <!-- Login Block Form -->
                                                            <div class="login-block-form">
                                                                <!-- Heading -->
                                                                <h4>Sign In to your Account</h4>
                                                                <!-- Border -->
                                                                <div class="bor bg-orange"></div>
                                                                <!-- Form -->
                                                                <form class="form" role="form">
                                                                    <!-- Form Group -->
                                                                    <div class="form-group">
                                                                        <!-- Label -->
                                                                        <label class="control-label" style="width:100%;">Email id</label>
                                                                        <!-- Input -->
                                                                        <input type="email" id="uemail1" class="form-control" placeholder="Enter Username">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label" style="width:100%;">Password</label>
                                                                        <input type="password" id="pass1" class="form-control" placeholder="Enter Password">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="checkbox checkbox-inline">
                                                                            <label>
                                                                                <input type="checkbox" id="remUserCredentials" onchange="rememberMe('uemail1','pass1',this);"> Remember
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group" style="text-align:center">
                                                                        <!-- Button -->
                                                                        <button type="button" id="signin1" class="btn btn-primary">Sign In</button>&nbsp;
                                                                        <button type="reset" class="btn btn-primary btn-inverse">Reset</button>
                                                                    </div>
                                                                    <div class="form-group" style="text-align:center">
                                                                        <a href="Account/ForgotPassword.html" class="black">Forget Password ?</a>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane fade" id="register-block2">
                                                            <div class="register-block-form">
                                                                <!-- Heading -->
                                                                <h4>Create the New Account</h4>
                                                                <!-- Border -->
                                                                <div class="bor bg-orange"></div>
                                                                <!-- Form -->
                                                                <form class="form" role="form">
                                                                    <!-- Form Group -->
                                                                    <div class="form-group">
                                                                        <!-- Label -->
                                                                        <label class="control-label">Name</label>
                                                                        <!-- Input -->
                                                                        <input type="text" id="txtname1" class="form-control" placeholder="Enter Name">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label">Email</label>
                                                                        <input type="email" id="txtemail1" class="form-control" placeholder="Enter Email">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label">Password</label>
                                                                        <input type="password" id="txtpass1" class="form-control" placeholder="Enter Password">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label">Confirm Password</label>
                                                                        <input type="password" id="txtcpass1" class="form-control" placeholder="Re-type password again">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label">Choose Country</label>
                                                                        <select class="form-control" id="mobileCountry">
                                                                            <option value="Kuwait" selected>+965 Kuwait</option>
                                                                            <option value="Bahrain">+973 Kingdom of Bahrain</option>
                                                                            <option value="Oman">+968 Sultanate of Oman</option>
                                                                            <option value="Qatar">+974 Qatar</option>
                                                                            <option value="SaudiArabia">+966 Kingdom of Saudi Arabia</option>
                                                                            <option value="UAE">+971 United arab emirates</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label">Mobile Number</label>
                                                                        <input type="number" id="txtmob1" class="form-control" placeholder="Mobile Number">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <!-- Checkbox -->
                                                                        <div class="checkbox">
                                                                            <label>
                                                                                <input type="checkbox"> By register, I read & accept  <a href="#">the terms</a>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <!-- Buton -->
                                                                        <button type="button" id="signup1" class="btn btn-primary">Sign Up</button>&nbsp;
                                                                        <button type="reset" class="btn btn-primary btn-inverse">Reset</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane fade" id="contact-block2">
                                                            <!-- Contact Block Form -->
                                                            <div class="contact-block-form">
                                                                <h4>Contact Form</h4>
                                                                <!-- Border -->
                                                                <div class="bor bg-orange"></div>
                                                                <!-- Form -->
                                                                <form class="form" role="form">
                                                                    <!-- Form Group -->
                                                                    <div class="form-group">
                                                                        <label class="control-label">Name</label>
                                                                        <input type="text" class="form-control" placeholder="Enter Name">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label">Email</label>
                                                                        <input type="text" class="form-control" placeholder="Enter Email">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label">Subject</label>
                                                                        <input type="text" class="form-control" placeholder="Enter Subject">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label for="comments" class="control-label">Comments</label>
                                                                        <textarea class="form-control" id="comments" rows="5" placeholder="Enter Comments"></textarea>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <!-- Buton -->
                                                                        <button type="submit" class="btn btn-primary">Submit</button>&nbsp;
                                                                        <button type="submit" class="btn btn-primary btn-inverse">Reset</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="introLoader" class="introLoading"></div>

                            <div class="container-wrapper">
                                <!-- start Header -->
                                <header id="header">
                                    <!-- start Navbar (Header) -->
                                    <!--<nav class="navbar navbar-default navbar-fixed-top navbar-sticky-function">
                                        <div class="container">
                                            <div class="logo-wrapper">
                                                <div class="logo">
                                                    <a href=" /?Length=5"><img src="~/images/logo.png" alt="Logo" style="width:68%;" /></a>
                                                </div>
                                            </div>
                                            <div id="navbar" class="navbar-nav-wrapper navbar-arrow">
                                                <ul class="nav navbar-nav" id="responsive-menu">
                                                    <li>
                                                        <a href="/"><font style="text-transform:uppercase">H</font>ome</a>

                                                    </li>
                                                    <li>
                                                        <a href="/About/Aboutus"><font style="text-transform:uppercase">A</font>bout</a>
                                                    </li>
                                                    <li>
                                                        <a href="#." data-toggle="modal" data-target="#addRestaurant"><font style="text-transform:uppercase">A</font>ddsss Restaurant</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="nav-mini-wrapper">
                                                <ul class="nav-mini sign-in">
                                                        <li><a href="#" data-toggle="modal" data-target="#myModal22" onclick="ad()">Login</a></li>
                                                        <li><a href="#" data-toggle="modal" data-target="#myModal22" onclick="af()">Register</a></li>

                                                </ul>
                                            </div>


                                        </div>

                                        <div id="slicknav-mobile"></div>
                                    </nav>-->
                                    <!-- end Navbar (Header) -->
									
                                    <div class="navbar navbar-default navbar-fixed-top">
									<div class="topnavbar">
										<ul class="navrightl">
											<li><a href=""><i class="fa fa-mobile"></i> Get The <span>App</span></a></li>
										</ul>
										<ul class="navrightb">
												<li class="borright"><a href="#" data-toggle="modal" data-target="#myModal22" onclick="ad()">Login</a></li>
												<li><a href="#" data-toggle="modal" data-target="#myModal22" onclick="af()">Register</a></li>
												<li class="safariBrowserLanDd"><div class="dropdown"> <button class="btn btn-default dropdown-toggle langDd safariBrowserlangDd" type="button" id="langDd" data-toggle="dropdown" style="text-transform: none;background-color:transparent;color:#fff;margin-top: 6px;padding:5px;">EN<span class="caret" style="border-top-color: #fbf8f8;"></span></button> <ul class="dropdown-menu" role="menu" aria-labelledby="menu1" style="background-color:#0c222d !important;"> <li role="presentation"><a role="menuitem" tabindex="-1" href="#." style="border:none;background-color:transparent;text-transform: none;">English</a></li><li role="presentation"><a role="menuitem" tabindex="-1" href="#." onclick="" style="border:none;background-color:transparent;text-transform: none;">عربي</a></li> </ul> </div> </li>

										</ul>
									</div>
                                        <div class="container">
                                            <div class="navbar-header">
                                                <button button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                                    <span class="sr-only">Toggle navigation</span>
                                                    <span class="icon-bar"></span>
                                                    <span class="icon-bar"></span>
                                                    <span class="icon-bar"></span>
                                                </button>
                                                <a href="index.php"><img class="logoWidth" src="images/logo.png" alt="Logo" /></a>
                                            </div>
                                            <div id="navbar" class="collapse navbar-collapse navbar-responsive-collapse">
                                                <div class="searchbarb1">
													<div class="s003">
													  <form>
														<div class="inner-form" id="searchbarff">
															<div class="input-field first-wrap">
																<div class="input-select">
																	<select data-trigger="" name="choices-single-defaul">
																		<option placeholder="">City</option>
																		<option>Kuwait City</option>
																		<option>Al Asimah</option>
																		<option>Hawalii</option>
																	</select>
																	<i class="fa fa-caret-down" aria-hidden="true"></i>
																</div>
															</div>
															<div class="input-field second-wrap">
																<i class="fa fa-search" aria-hidden="true"></i>
																<input id="search" type="text" placeholder="Search for restaurants or cuisines" />
															</div>
															<div class="input-field third-wrap">
																<button class="btn-search" type="button">
																	Search
																</button>
															</div>
														</div>
													  </form>
													</div>
												</div>
												<!--
												<ul class="nav navbar-nav menuCenterClass">
                                                    <li>
                                                        <a href="#"><font style="text-transform:uppercase">H</font>ome</a>
                                                    </li>
                                                    <li>
                                                        <a href="#"><font style="text-transform:uppercase">A</font>bout</a>
                                                    </li>
                                                    <li>
                                                        <a href="#" data-toggle="modal" data-target="#addRestaurant"><font style="text-transform:uppercase">A</font>dd restaurant</a>
                                                    </li>
                                                </ul>
                                                -->
                                            </div>
                                        </div>
                                    </div>
                                </header>

                                <div class="main-wrapper" style="background-color: #fff;">
<style>
    a {
        color: #fff;
        transition: all 0.3s ease;
        -webkit-transition: all 0.3s ease;
        -moz-transition: all 0.3s ease;
        cursor: poitner;
        text-decoration: none;
        font-weight: 600;
    }

    .btn-primary {
        color: #fff;
        border: 0;
        background-color: none;
        /* border-color: #2e6da4; */
    }

    .btn {
        -webkit-transition: all .3s;
        -o-transition: all .3s;
        transition: all .3s;
        text-transform: uppercase;
        font-size: 11px;
        letter-spacing: 2px;
        padding-top: 9px;
        padding-bottom: 3px;
        font-weight: 700;
        margin-top: 3px;
    }

    .orderLtr {
        color: #fff !important;
        cursor: default !important;
    }

    .overlay {
        height: 0%;
        width: 100%;
        position: fixed;
        z-index: 999;
        top: 8%;
        left: 0;
        background-color: #f6f6f6;
        //background-color: rgba(0,0,0, 0.9);
        overflow-y: scroll;
        transition: 0.5s;
    }

    .overlay-content {
        position: relative;
        top: 15%;
        width: 100%;
        text-align: center;
        margin-top: 30px;
    }

    .overlay .closebtn {
        top: 10%;
        left: 30px;
        font-size: 60px;
        color: #fc204e;
        z-index: 99999;
    }

    .absoluteClass {
        position: absolute;
    }

    .fixedClass {
        position: fixed;
    }

    .overlay .closebtn:hover {
        color: #fc204e;
    }.form-group {
        position: relative;
        margin-bottom: 1.5rem;
    }

    .form-control-placeholder {
        position: absolute;
        top: 0;
        padding: 7px 0 0 13px;
        transition: all 200ms;
        opacity: 0.5;
    }

    .form-control:focus + .form-control-placeholder,
    .form-control:valid + .form-control-placeholder {
        font-size: 75%;
        transform: translate3d(0, -100%, 0);
        opacity: 1;
    }

    input[type='radio'], input[type='checkbox'] {
        display: block !important;
        float: left;
        width: 18px;
        /* margin-top: 1px; */
    }

    
    .selector-control {
        font-size: 14px;
        line-height: 30px;
        padding-left: 5px;
        padding-right: 5px;
    }
    .orderLtr{
       color:#fff !important;
       cursor:default !important;
    }
.checkBoxCss {
    border: 1px solid #000;
    width: 15px !important;
    height: 15px !important;
    position: absolute;
    margin-top: 4px\9;
    cursor:pointer;
    border-radius:0px !important;
}

.lblCss {
    font-size: 14px;
    font-weight: 400;
    letter-spacing: 0.04em;
    margin-top: 4px;
    cursor: pointer;
    margin-left: 20px;
}
.boldCss{
    font-weight:600 !important;
}
.hrCss {
    border-top: 1px solid #dcd2d2 !important;
}
.control {
    display: block;
    position: relative;
    padding-left: 25px;
    margin-bottom: 15px;
    cursor: pointer;
    padding-top: 1px;margin-left: 0px;font-family: titilium;
}

    .control input {
        position: absolute;
        z-index: -1;
        opacity: 0;
    }

.control__indicator {
    position: absolute;
    top: 0px;
    left: 0;
    height: 17px;
    width: 18px;
    background: transparent;
    border: 1px solid gray;
}

.control:hover input ~ .control__indicator,
.control input:focus ~ .control__indicator {
    background: transparent;
}

.control input:checked ~ .control__indicator {
    background: #fc204e;
    border:none;
}

.control__indicator:after {
    content: '';
    position: absolute;
    display: none;
}

.control input:checked ~ .control__indicator:after {
    display: block;
}

.control--checkbox .control__indicator:after {
    left: 6px;
    top: 2px;
    width: 6px;
    height: 11px;
    border: solid #fff;
    border-width: 0 2px 2px 0;
    transform: rotate(45deg);
}

.control--checkbox input:disabled ~ .control__indicator:after {
    border-color: #7b7b7b;
}
.search-icon {
    left: 16px;
    background-image: url(https://api.bildarb.com/content/images/icon-search-btn-black.svg);
}
input[type=text] {
    padding-left: 25px;
}
.mui-textfield {
    display: block;
    padding-top: 4px;
    margin-bottom: 4px;
    position: relative;
}
.search-icon {
    position: absolute;
    top: 28%;
    width: 20px;
    height: 20px;
    background-size: 100%;
    background-repeat: no-repeat;
}
.mui-textfield > input {
    height: 32px;
}
.mui-textfield > input, .mui-textfield > textarea {
    -webkit-animation-duration: .1ms;
    animation-duration: .1ms;
    -webkit-animation-name: mui-node-inserted;
    animation-name: mui-node-inserted;
    display: block;
    background-color: transparent;
    color: rgba(0,0,0,.87);
    border: none;
    border-bottom: 1px solid rgba(0,0,0,.26);
    outline: 0;
    width: 100%;
    font-size: 16px;
    padding: 0;
    box-shadow: none;
    border-radius: 0;
    background-image: none;
}
.close-icon {
    right: 16px;
    background-image: url(https://api.bildarb.com/content/images/close.svg);
    cursor: pointer;
    transition: opacity .2s ease-in-out;
}
.close-icon, .search-icon {
    position: absolute;
    top: 28%;
    width: 20px;
    height: 20px;
    background-size: 100%;
    background-repeat: no-repeat;
}
.searchTxtBox:focus{
    box-shadow:none;
}

        label.btn span {
            font-size: 1.5em;
        }

        label input[type="radio"] ~ i.fa.fa-circle-o {
            color: #fc204e;
            display: inline;
        }

        label input[type="radio"] ~ i.fa.fa-dot-circle-o {
            display: none;
        }

        label input[type="radio"]:checked ~ i.fa.fa-circle-o {
            display: none;
        }

        label input[type="radio"]:checked ~ i.fa.fa-dot-circle-o {
            color: #fc204e;
            display: inline;
        }

        label:hover input[type="radio"] ~ i.fa {
            color: #fc204e;
        }

        label input[type="checkbox"] ~ i.fa.fa-square-o {
            color: #fc204e;
            display: inline;
        }

        label input[type="checkbox"] ~ i.fa.fa-check-square-o {
            display: none;
        }

        label input[type="checkbox"]:checked ~ i.fa.fa-square-o {
            display: none;
        }

        label input[type="checkbox"]:checked ~ i.fa.fa-check-square-o {
            color: #fc204e;
            display: inline;
        }

        label:hover input[type="checkbox"] ~ i.fa {
            color: #fc204e;
        }

        div[data-toggle="buttons"] label.active {
            color: #fc204e;
        }

        div[data-toggle="buttons"] label {
            display: inline-block;
            padding: 6px 12px;
            margin-bottom: 0;
            font-size: 14px;
            font-weight: normal;
            line-height: 2em;
            text-align: left;
            white-space: nowrap;
            vertical-align: top;
            cursor: pointer;
            background-color: none;
            border: 0px solid #c8c8c8;
            border-radius: 3px;
            color: #fc204e;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            -o-user-select: none;
            user-select: none;
        }

            div[data-toggle="buttons"] label:hover {
                color: #7AA3CC;
            }

            div[data-toggle="buttons"] label:active, div[data-toggle="buttons"] label.active {
                -webkit-box-shadow: none;
                box-shadow: none;
            }

        .btn-circle {
            width: 30px;
            height: 30px;
            text-align: center;
            padding: 6px 0;
            font-size: 12px;
            line-height: 1.428571429;
            border-radius: 15px;
        }

            .btn-circle.btn-lg {
                float: left;
                width: 40px;
                height: 40px;
                padding: 10px 16px;
                font-size: 14px;
                line-height: 1.33;
                border-radius: 25px;
            }

            .btn-circle.btn-xl {
                width: 70px;
                height: 70px;
                padding: 10px 16px;
                font-size: 24px;
                line-height: 1.33;
                border-radius: 35px;
            }

        a:hover, a:focus {
            color: #fc204e;
            text-decoration: none !important;
        }

        .btn-primary:hover, .btn-primary:focus, .btn-primary:active, .btn-primary.active, .open .dropdown-toggle.btn-primary {
            color: #fff;
            background-color: #fc204e !important;
        }

        .btn-primary:hover, .btn-primary:focus, .btn-primary:active, .btn-primary.active, .open .dropdown-toggle.btn-primary {
            color: #fff;
            background-color: #fc204e;
            border-color: #fc204e;
        }

        .btn-image:before {
            content: "";
            width: 22px;
            margin-bottom: -2px;
            height: 18px;
            display: inline-block;
            background-color: transparent;
            background-repeat: no-repeat;
        }

        .btnPickup, .btnReservation, .btnPreorder {
            margin-top: 5px !important;
            padding: 5px 10px 6px 10px !important;
        }

            .btnPickup:before {
                background-image: url(images/icns/pickup1.png);
                background-size: 90% 90%;
                background-position-y: 2px;
            }

            .btnPreorder:before {
                background-image: url(images/icns/preorder.png);
                background-size: 90% 90%;
                background-position-y: 2px;
            }

            .btnReservation:before {
                background-image: url(images/icns/reservation1.png);
                background-size: 91% 95%;
                background-position-y: 3px;
            }

        .rcross:after {
            font-weight: bold;
            font-size: larger;
            content: '';
            margin-top: -13px;
            margin-right: -24px;
            margin-left: 542px;
            z-index: 99999999999;
            display: block;
            height: 20px;
            width: 20px;
            position: absolute;
            background: url(images/cross.png) no-repeat 0px 0px;
        }

        .txtBoxcss {
            padding: 12px 4px !important;
        }

        #RestaurantRequestForm {
            padding-top: 36px;
        }
    </style>
<script defer src="../cdn.rawgit.com/chrisveness/geodesy/v1.1.2/latlon-spherical.js"></script>
<script defer src="../cdn.rawgit.com/chrisveness/geodesy/v1.1.2/dms.js"></script>
<div class="modal fade padding-30" role="dialog" id="displayAllBranches">

    <div class="modal-dialog" style="left:64px !important;">
        <div class="modal-content" style="backface-visibility:hidden;width:67%">
            <div class="modal-header" style="border-radius:6px !important;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="branchHeadingType"></h4>
            </div>
            <div class="modal-body" id="restaurantBranchesList">

            </div>
            <div class="modal-footer" style="border:0px !important;padding:0px !important; margin-bottom:0px !important;">

            </div>
        </div>

    </div>
</div>




<div id="myNav" class="overlay">
    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()"> &times;</a>
    <div class="overlay-content" style="padding-bottom: 5%;">
        <div class="container">
            <div class="row" id="filterPopUp"> </div>
        </div>
    </div>
</div>