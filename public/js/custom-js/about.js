var $window = $( window ); // 1. Window Object.
var $featuredMedia = $( "#videoBlock" ); // 1. The Video Container.
var $featuredVideo = $( "#video" ); // 2. The Video object container.
//var vid = document.getElementById("video");
  
$(document).ready(function(){   
    $window.on( "scroll", function() {
    	//&& !vid.paused
        if(!inViewportCenter($featuredMedia)) {
            $featuredVideo.addClass('is-sticky');
        } else {
            $featuredVideo.removeClass('is-sticky');
        }
    });

});
