<?php include 'header.php'; ?>
	<div class="searchquick3 bktab" style="padding-bottom:0px;">
	<div class="container">
		<h6 class="logo-content regularweb1">Home &gt; Peacock &gt; Book A Table At Peacock</h6>
		<div class="col-md-8">
			<div class="cardd">
				<div class="col-md-2">
					<img src="content/images/demo.png">
				</div>
				<div class="col-md-10">
					<div class="elight"><h3>Peacock Restaurant</h3>
						<!-- <span class="rating"><span>4.3</span>/5<span class="votes">1010 votes</span></span> -->
					</div>
					<p class="light">Kuwait City <span class="dots">.</span> Casual Dining, Bar</p>
					<p>
						<span class="opennow">Open now</span> <span class="dots">.</span> Morden Indian <span class="dots">
						<!-- .</span> Costs Rs. <span class="bigf">2500</span> for two -->
					</p>
				</div>
			</div>
			<div class="cardddd">
				<ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#home">Reservation</a></li>
                    <li><a data-toggle="tab" href="#menu1">View On Map</a></li>
                </ul>
                <div class="tab-content tablab">
                	<div id="home" class="tab-pane fade in active">
						<h4>1. Please select your booking details</h4>
						<br/>
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label for="usr">Select a date:</label>
									<select class="datepic form-control">
										<option selected="" value="24/11/2018">Today, Sat, 24 November</option>
										<option value="25/11/2018">Tomorrow, Sun, 25 November</option>
										<option value="26/11/2018">Mon, 26 November</option>
										<option value="27/11/2018">Tue, 27 November</option>
										<option value="28/11/2018">Wed, 28 November</option>
										<option value="29/11/2018">Thu, 29 November</option>
										<option value="30/11/2018">Fri, 30 November</option>
									</select>
								</div>	
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label for="usr">Number of Guests:</label>
									<select class="datepic form-control">
									    <option value="">Select Guests</option>
										<option value="2" selected="selected">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
										<option value="6">6</option>
										<option value="7">7</option>
										<option value="8">8</option>
									</select>
								</div>	
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label for="usr">Time:</label>
									<select class="datepic form-control">
										<option value="">Select Time</option>
										<option value="14:00:00" selected="">2:00 PM</option>
										<option value="14:30:00">2:30 PM</option>
										<option value="19:00:00">7:00 PM</option>
										<option value="19:30:00">7:30 PM</option>
										<option value="20:00:00">8:00 PM</option>
										<option value="20:30:00">8:30 PM</option>
										<option value="22:30:00">10:30 PM</option>
										<option value="23:00:00">11:00 PM</option>
									</select>					
								</div>	
							</div>
							<div class="clearfix"></div>
							<br/>
							<div class="col-md-12">
								<div class="form-group radio_btn">
									<div class="col-md-12 pad_zero">
										<label for="usr">Choose Table</label>
									</div>
									<div class="col-md-6">
										<label class="radio-inline"><input type="radio" name="optradio" checked>Outdoor</label>
									</div>
									<div class="col-md-6">
										<label class="radio-inline"><input type="radio" name="optradio">Indoors</label>
									</div>
								</div>
							</div>
							<div class="col-md-12 how_it_works">
								<div>
									<h4>How it works</h4>
									<p>We will contact the restaurant content your behalf to book a table and get back to you shortly via email and SMS</p>
								</div>
							</div>
							<div class="col-md-12 addit_req">
								<div>
									<h4>Additional request</h4>
									<p>Let us know if there are any additional requests that we should convey to the restaurant on your behalf</p>
								</div>
								<div class="form-group">
									<textarea class="form-control" rows="3" id="comment" placeholder="Please write additional request"></textarea>
								</div> 
							</div>
						</div>	
						<!--
						<h4>2. Enter guest details</h4>
						<br />
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
								  <label for="usr">First Name:</label>
								  <input type="text" class="form-control datepic" tabindex="1" id="usr">
								</div>
								<div class="form-group">
								  <label for="eml">Email Id:</label>
								  <input type="email" class="form-control datepic" tabindex="3" id="eml">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
								  <label for="usr1">Last Name:</label>
								  <input type="text" class="form-control datepic" tabindex="2" id="usr1">
								</div>	
								<div class="form-group phonenum">
									<label for="usr1">Phone number</label>
									<div class="phone-list">
										<div class="input-group phone-input">
											<span class="input-group-btn">
												<input type="text" class="form-control datepic phonenumin" tabindex="4" placeholder="+91" />	
											</span>
											<input type="text" class="form-control datepic" tabindex="5" placeholder="9672200143" />
										</div>
									</div>					
								</div>						
							</div>
						</div>	
						-->			
						<div class="row">
							<div class="col-md-12">
								<input type="button" class="borderBtn"  tabindex="6" value="Book" data-toggle="modal" data-target="#reservationbook">
								<!-- <p>It may take upto 3 mins</p> -->
							</div>
						</div>	
					</div>
					<div id="menu1" class="tab-pane fade">
                        <div id="map" style="height:400px"></div>
                    </div>
				</div>	
			</div>
		</div>
		<div class="col-md-4">
			<div class="carddx1">
				<h4>GET THE APP!</h4>
				<p>Reserve tables & manage your reservations right from your phone!</p>
				<img src="images/store.png" />
				<h4 class="text-center">Available On</h4>
				<p class="text-center icons"><a href="https://play.google.com/store/apps/details?id=bildarb.desind.com" target="_blank"><i class="fa fa-android"></i></a><a href="https://itunes.apple.com/us/app/bildarb/id1349443656?ls=1&mt=8" target="_blank"><i class="fa fa-apple"></i></a></p>
				<h4 class="text-center downloadlink">Download link via sms</h4>
			</div>
		</div>
	</div>
</div>
<?php include 'footer.php'; ?>
<!-- book popup for verifying -->
<div id="reservationbook" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title text-center">Agarwal Caterers</h4>
      </div>
      <div class="modal-body">
        <div class="col-md-12">
			<div class="col-md-6">
				<p>Booking Date: 03-12-2018</p>
			</div>
			<div class="col-md-6">
				<p>Booking Time: 7:30pm</p>
			</div>
			<div class="col-md-6">
				<p>Number of guests: 4</p>
			</div>
			<div class="col-md-6">
				<p>Table: Indoors</p>
			</div>
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-toggle="modal" data-dismiss="modal" data-target="#booktable">Book Table</button>
      </div>
    </div>

  </div>
</div>
<!-- book popup for verifying -->
<!-- book table -->
<div id="booktable" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title text-center">&nbsp;</h4>
      </div>
      <div class="modal-body">
        <img src="images/righticon.png" />
		<h3 class="modal-title text-center">Thanks for choosing our services</h3>
		<p class="text-center">Your table has been booked</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default"data-dismiss="modal">Ok</button>
      </div>
    </div>

  </div>
</div>
<!-- book table -->
